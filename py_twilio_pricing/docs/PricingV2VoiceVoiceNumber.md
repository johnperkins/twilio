# PricingV2VoiceVoiceNumber


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**destination_number** | **str** | The destination phone number, in E.164 format | [optional] 
**origination_number** | **str** | The origination phone number, in E.164 format | [optional] 
**country** | **str** | The name of the country | [optional] 
**iso_country** | **str** | The ISO country code | [optional] 
**outbound_call_prices** | [**List[PricingV2VoiceVoiceNumberOutboundCallPricesInner]**](PricingV2VoiceVoiceNumberOutboundCallPricesInner.md) | The list of OutboundCallPriceWithOrigin records | [optional] 
**inbound_call_price** | [**PricingV2VoiceVoiceNumberInboundCallPrice**](PricingV2VoiceVoiceNumberInboundCallPrice.md) |  | [optional] 
**price_unit** | **str** | The currency in which prices are measured, in ISO 4127 format (e.g. usd, eur, jpy) | [optional] 
**url** | **str** | The absolute URL of the resource | [optional] 

## Example

```python
from py_twilio_pricing.models.pricing_v2_voice_voice_number import PricingV2VoiceVoiceNumber

# TODO update the JSON string below
json = "{}"
# create an instance of PricingV2VoiceVoiceNumber from a JSON string
pricing_v2_voice_voice_number_instance = PricingV2VoiceVoiceNumber.from_json(json)
# print the JSON string representation of the object
print PricingV2VoiceVoiceNumber.to_json()

# convert the object into a dict
pricing_v2_voice_voice_number_dict = pricing_v2_voice_voice_number_instance.to_dict()
# create an instance of PricingV2VoiceVoiceNumber from a dict
pricing_v2_voice_voice_number_form_dict = pricing_v2_voice_voice_number.from_dict(pricing_v2_voice_voice_number_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


