# PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**origination_prefixes** | **List[str]** |  | [optional] 
**destination_prefixes** | **List[str]** |  | [optional] 
**base_price** | **float** |  | [optional] 
**current_price** | **float** |  | [optional] 
**friendly_name** | **str** |  | [optional] 

## Example

```python
from py_twilio_pricing.models.pricing_v2_trunking_country_instance_terminating_prefix_prices_inner import PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner

# TODO update the JSON string below
json = "{}"
# create an instance of PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner from a JSON string
pricing_v2_trunking_country_instance_terminating_prefix_prices_inner_instance = PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner.from_json(json)
# print the JSON string representation of the object
print PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner.to_json()

# convert the object into a dict
pricing_v2_trunking_country_instance_terminating_prefix_prices_inner_dict = pricing_v2_trunking_country_instance_terminating_prefix_prices_inner_instance.to_dict()
# create an instance of PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner from a dict
pricing_v2_trunking_country_instance_terminating_prefix_prices_inner_form_dict = pricing_v2_trunking_country_instance_terminating_prefix_prices_inner.from_dict(pricing_v2_trunking_country_instance_terminating_prefix_prices_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


