# PricingV2TrunkingCountryInstanceOriginatingCallPricesInner


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**base_price** | **float** |  | [optional] 
**current_price** | **float** |  | [optional] 
**number_type** | **str** |  | [optional] 

## Example

```python
from py_twilio_pricing.models.pricing_v2_trunking_country_instance_originating_call_prices_inner import PricingV2TrunkingCountryInstanceOriginatingCallPricesInner

# TODO update the JSON string below
json = "{}"
# create an instance of PricingV2TrunkingCountryInstanceOriginatingCallPricesInner from a JSON string
pricing_v2_trunking_country_instance_originating_call_prices_inner_instance = PricingV2TrunkingCountryInstanceOriginatingCallPricesInner.from_json(json)
# print the JSON string representation of the object
print PricingV2TrunkingCountryInstanceOriginatingCallPricesInner.to_json()

# convert the object into a dict
pricing_v2_trunking_country_instance_originating_call_prices_inner_dict = pricing_v2_trunking_country_instance_originating_call_prices_inner_instance.to_dict()
# create an instance of PricingV2TrunkingCountryInstanceOriginatingCallPricesInner from a dict
pricing_v2_trunking_country_instance_originating_call_prices_inner_form_dict = pricing_v2_trunking_country_instance_originating_call_prices_inner.from_dict(pricing_v2_trunking_country_instance_originating_call_prices_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


