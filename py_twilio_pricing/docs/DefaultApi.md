# py_twilio_pricing.DefaultApi

All URIs are relative to *https://pricing.twilio.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**fetch_trunking_country**](DefaultApi.md#fetch_trunking_country) | **GET** /v2/Trunking/Countries/{IsoCountry} | 
[**fetch_trunking_number**](DefaultApi.md#fetch_trunking_number) | **GET** /v2/Trunking/Numbers/{DestinationNumber} | 
[**fetch_voice_country**](DefaultApi.md#fetch_voice_country) | **GET** /v2/Voice/Countries/{IsoCountry} | 
[**fetch_voice_number**](DefaultApi.md#fetch_voice_number) | **GET** /v2/Voice/Numbers/{DestinationNumber} | 
[**list_trunking_country**](DefaultApi.md#list_trunking_country) | **GET** /v2/Trunking/Countries | 
[**list_voice_country**](DefaultApi.md#list_voice_country) | **GET** /v2/Voice/Countries | 


# **fetch_trunking_country**
> PricingV2TrunkingCountryInstance fetch_trunking_country(iso_country)



Fetch a specific Country.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_pricing
from py_twilio_pricing.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://pricing.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_pricing.Configuration(
    host = "https://pricing.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_pricing.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_pricing.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_pricing.DefaultApi(api_client)
    iso_country = 'iso_country_example' # str | The [ISO country code](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) of the origin-based voice pricing information to fetch.

    try:
        api_response = api_instance.fetch_trunking_country(iso_country)
        print("The response of DefaultApi->fetch_trunking_country:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_trunking_country: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iso_country** | **str**| The [ISO country code](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) of the origin-based voice pricing information to fetch. | 

### Return type

[**PricingV2TrunkingCountryInstance**](PricingV2TrunkingCountryInstance.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_trunking_number**
> PricingV2TrunkingNumber fetch_trunking_number(destination_number, origination_number=origination_number)



Fetch pricing information for a specific destination and, optionally, origination phone number.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_pricing
from py_twilio_pricing.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://pricing.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_pricing.Configuration(
    host = "https://pricing.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_pricing.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_pricing.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_pricing.DefaultApi(api_client)
    destination_number = 'destination_number_example' # str | The destination phone number, in [E.164](https://www.twilio.com/docs/glossary/what-e164) format, for which to fetch the origin-based voice pricing information. E.164 format consists of a + followed by the country code and subscriber number.
    origination_number = 'origination_number_example' # str | The origination phone number, in [E.164](https://www.twilio.com/docs/glossary/what-e164) format, for which to fetch the origin-based voice pricing information. E.164 format consists of a + followed by the country code and subscriber number. (optional)

    try:
        api_response = api_instance.fetch_trunking_number(destination_number, origination_number=origination_number)
        print("The response of DefaultApi->fetch_trunking_number:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_trunking_number: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **destination_number** | **str**| The destination phone number, in [E.164](https://www.twilio.com/docs/glossary/what-e164) format, for which to fetch the origin-based voice pricing information. E.164 format consists of a + followed by the country code and subscriber number. | 
 **origination_number** | **str**| The origination phone number, in [E.164](https://www.twilio.com/docs/glossary/what-e164) format, for which to fetch the origin-based voice pricing information. E.164 format consists of a + followed by the country code and subscriber number. | [optional] 

### Return type

[**PricingV2TrunkingNumber**](PricingV2TrunkingNumber.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_voice_country**
> PricingV2VoiceVoiceCountryInstance fetch_voice_country(iso_country)



Fetch a specific Country.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_pricing
from py_twilio_pricing.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://pricing.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_pricing.Configuration(
    host = "https://pricing.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_pricing.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_pricing.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_pricing.DefaultApi(api_client)
    iso_country = 'iso_country_example' # str | The [ISO country code](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) of the origin-based voice pricing information to fetch.

    try:
        api_response = api_instance.fetch_voice_country(iso_country)
        print("The response of DefaultApi->fetch_voice_country:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_voice_country: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **iso_country** | **str**| The [ISO country code](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) of the origin-based voice pricing information to fetch. | 

### Return type

[**PricingV2VoiceVoiceCountryInstance**](PricingV2VoiceVoiceCountryInstance.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_voice_number**
> PricingV2VoiceVoiceNumber fetch_voice_number(destination_number, origination_number=origination_number)



Fetch pricing information for a specific destination and, optionally, origination phone number.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_pricing
from py_twilio_pricing.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://pricing.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_pricing.Configuration(
    host = "https://pricing.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_pricing.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_pricing.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_pricing.DefaultApi(api_client)
    destination_number = 'destination_number_example' # str | The destination phone number, in [E.164](https://www.twilio.com/docs/glossary/what-e164) format, for which to fetch the origin-based voice pricing information. E.164 format consists of a + followed by the country code and subscriber number.
    origination_number = 'origination_number_example' # str | The origination phone number, in [E.164](https://www.twilio.com/docs/glossary/what-e164) format, for which to fetch the origin-based voice pricing information. E.164 format consists of a + followed by the country code and subscriber number. (optional)

    try:
        api_response = api_instance.fetch_voice_number(destination_number, origination_number=origination_number)
        print("The response of DefaultApi->fetch_voice_number:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_voice_number: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **destination_number** | **str**| The destination phone number, in [E.164](https://www.twilio.com/docs/glossary/what-e164) format, for which to fetch the origin-based voice pricing information. E.164 format consists of a + followed by the country code and subscriber number. | 
 **origination_number** | **str**| The origination phone number, in [E.164](https://www.twilio.com/docs/glossary/what-e164) format, for which to fetch the origin-based voice pricing information. E.164 format consists of a + followed by the country code and subscriber number. | [optional] 

### Return type

[**PricingV2VoiceVoiceNumber**](PricingV2VoiceVoiceNumber.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_trunking_country**
> ListTrunkingCountryResponse list_trunking_country(page_size=page_size)





### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_pricing
from py_twilio_pricing.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://pricing.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_pricing.Configuration(
    host = "https://pricing.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_pricing.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_pricing.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_pricing.DefaultApi(api_client)
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_trunking_country(page_size=page_size)
        print("The response of DefaultApi->list_trunking_country:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_trunking_country: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListTrunkingCountryResponse**](ListTrunkingCountryResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_voice_country**
> ListVoiceCountryResponse list_voice_country(page_size=page_size)





### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_pricing
from py_twilio_pricing.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://pricing.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_pricing.Configuration(
    host = "https://pricing.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_pricing.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_pricing.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_pricing.DefaultApi(api_client)
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_voice_country(page_size=page_size)
        print("The response of DefaultApi->list_voice_country:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_voice_country: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListVoiceCountryResponse**](ListVoiceCountryResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

