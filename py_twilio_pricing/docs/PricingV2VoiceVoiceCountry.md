# PricingV2VoiceVoiceCountry


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **str** | The name of the country | [optional] 
**iso_country** | **str** | The ISO country code | [optional] 
**url** | **str** | The absolute URL of the resource | [optional] 

## Example

```python
from py_twilio_pricing.models.pricing_v2_voice_voice_country import PricingV2VoiceVoiceCountry

# TODO update the JSON string below
json = "{}"
# create an instance of PricingV2VoiceVoiceCountry from a JSON string
pricing_v2_voice_voice_country_instance = PricingV2VoiceVoiceCountry.from_json(json)
# print the JSON string representation of the object
print PricingV2VoiceVoiceCountry.to_json()

# convert the object into a dict
pricing_v2_voice_voice_country_dict = pricing_v2_voice_voice_country_instance.to_dict()
# create an instance of PricingV2VoiceVoiceCountry from a dict
pricing_v2_voice_voice_country_form_dict = pricing_v2_voice_voice_country.from_dict(pricing_v2_voice_voice_country_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


