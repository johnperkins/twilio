# PricingV2VoiceVoiceNumberOutboundCallPricesInner


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**base_price** | **float** |  | [optional] 
**current_price** | **float** |  | [optional] 
**origination_prefixes** | **List[str]** |  | [optional] 

## Example

```python
from py_twilio_pricing.models.pricing_v2_voice_voice_number_outbound_call_prices_inner import PricingV2VoiceVoiceNumberOutboundCallPricesInner

# TODO update the JSON string below
json = "{}"
# create an instance of PricingV2VoiceVoiceNumberOutboundCallPricesInner from a JSON string
pricing_v2_voice_voice_number_outbound_call_prices_inner_instance = PricingV2VoiceVoiceNumberOutboundCallPricesInner.from_json(json)
# print the JSON string representation of the object
print PricingV2VoiceVoiceNumberOutboundCallPricesInner.to_json()

# convert the object into a dict
pricing_v2_voice_voice_number_outbound_call_prices_inner_dict = pricing_v2_voice_voice_number_outbound_call_prices_inner_instance.to_dict()
# create an instance of PricingV2VoiceVoiceNumberOutboundCallPricesInner from a dict
pricing_v2_voice_voice_number_outbound_call_prices_inner_form_dict = pricing_v2_voice_voice_number_outbound_call_prices_inner.from_dict(pricing_v2_voice_voice_number_outbound_call_prices_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


