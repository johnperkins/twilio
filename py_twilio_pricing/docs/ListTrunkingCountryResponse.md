# ListTrunkingCountryResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**countries** | [**List[PricingV2TrunkingCountry]**](PricingV2TrunkingCountry.md) |  | [optional] 
**meta** | [**ListTrunkingCountryResponseMeta**](ListTrunkingCountryResponseMeta.md) |  | [optional] 

## Example

```python
from py_twilio_pricing.models.list_trunking_country_response import ListTrunkingCountryResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ListTrunkingCountryResponse from a JSON string
list_trunking_country_response_instance = ListTrunkingCountryResponse.from_json(json)
# print the JSON string representation of the object
print ListTrunkingCountryResponse.to_json()

# convert the object into a dict
list_trunking_country_response_dict = list_trunking_country_response_instance.to_dict()
# create an instance of ListTrunkingCountryResponse from a dict
list_trunking_country_response_form_dict = list_trunking_country_response.from_dict(list_trunking_country_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


