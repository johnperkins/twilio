# PricingV2Voice


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | The resource name | [optional] 
**url** | **str** | The absolute URL of the resource | [optional] 
**links** | **object** | The URLs of the related Countries and Numbers resources | [optional] 

## Example

```python
from py_twilio_pricing.models.pricing_v2_voice import PricingV2Voice

# TODO update the JSON string below
json = "{}"
# create an instance of PricingV2Voice from a JSON string
pricing_v2_voice_instance = PricingV2Voice.from_json(json)
# print the JSON string representation of the object
print PricingV2Voice.to_json()

# convert the object into a dict
pricing_v2_voice_dict = pricing_v2_voice_instance.to_dict()
# create an instance of PricingV2Voice from a dict
pricing_v2_voice_form_dict = pricing_v2_voice.from_dict(pricing_v2_voice_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


