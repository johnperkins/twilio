# PricingV2TrunkingNumberOriginatingCallPrice

The OriginatingCallPrice record

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**base_price** | **float** |  | [optional] 
**current_price** | **float** |  | [optional] 
**number_type** | **str** |  | [optional] 

## Example

```python
from py_twilio_pricing.models.pricing_v2_trunking_number_originating_call_price import PricingV2TrunkingNumberOriginatingCallPrice

# TODO update the JSON string below
json = "{}"
# create an instance of PricingV2TrunkingNumberOriginatingCallPrice from a JSON string
pricing_v2_trunking_number_originating_call_price_instance = PricingV2TrunkingNumberOriginatingCallPrice.from_json(json)
# print the JSON string representation of the object
print PricingV2TrunkingNumberOriginatingCallPrice.to_json()

# convert the object into a dict
pricing_v2_trunking_number_originating_call_price_dict = pricing_v2_trunking_number_originating_call_price_instance.to_dict()
# create an instance of PricingV2TrunkingNumberOriginatingCallPrice from a dict
pricing_v2_trunking_number_originating_call_price_form_dict = pricing_v2_trunking_number_originating_call_price.from_dict(pricing_v2_trunking_number_originating_call_price_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


