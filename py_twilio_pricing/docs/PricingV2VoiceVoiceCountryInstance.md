# PricingV2VoiceVoiceCountryInstance


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | **str** | The name of the country | [optional] 
**iso_country** | **str** | The ISO country code | [optional] 
**outbound_prefix_prices** | [**List[PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner]**](PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner.md) | The list of OutboundPrefixPriceWithOrigin records | [optional] 
**inbound_call_prices** | [**List[PricingV2TrunkingCountryInstanceOriginatingCallPricesInner]**](PricingV2TrunkingCountryInstanceOriginatingCallPricesInner.md) | The list of InboundCallPrice records | [optional] 
**price_unit** | **str** | The currency in which prices are measured, in ISO 4127 format (e.g. usd, eur, jpy) | [optional] 
**url** | **str** | The absolute URL of the resource | [optional] 

## Example

```python
from py_twilio_pricing.models.pricing_v2_voice_voice_country_instance import PricingV2VoiceVoiceCountryInstance

# TODO update the JSON string below
json = "{}"
# create an instance of PricingV2VoiceVoiceCountryInstance from a JSON string
pricing_v2_voice_voice_country_instance_instance = PricingV2VoiceVoiceCountryInstance.from_json(json)
# print the JSON string representation of the object
print PricingV2VoiceVoiceCountryInstance.to_json()

# convert the object into a dict
pricing_v2_voice_voice_country_instance_dict = pricing_v2_voice_voice_country_instance_instance.to_dict()
# create an instance of PricingV2VoiceVoiceCountryInstance from a dict
pricing_v2_voice_voice_country_instance_form_dict = pricing_v2_voice_voice_country_instance.from_dict(pricing_v2_voice_voice_country_instance_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


