# ListVoiceCountryResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**countries** | [**List[PricingV2VoiceVoiceCountry]**](PricingV2VoiceVoiceCountry.md) |  | [optional] 
**meta** | [**ListTrunkingCountryResponseMeta**](ListTrunkingCountryResponseMeta.md) |  | [optional] 

## Example

```python
from py_twilio_pricing.models.list_voice_country_response import ListVoiceCountryResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ListVoiceCountryResponse from a JSON string
list_voice_country_response_instance = ListVoiceCountryResponse.from_json(json)
# print the JSON string representation of the object
print ListVoiceCountryResponse.to_json()

# convert the object into a dict
list_voice_country_response_dict = list_voice_country_response_instance.to_dict()
# create an instance of ListVoiceCountryResponse from a dict
list_voice_country_response_form_dict = list_voice_country_response.from_dict(list_voice_country_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


