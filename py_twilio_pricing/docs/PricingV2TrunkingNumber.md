# PricingV2TrunkingNumber


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**destination_number** | **str** | The destination phone number, in E.164 format | [optional] 
**origination_number** | **str** | The origination phone number, in E.164 format | [optional] 
**country** | **str** | The name of the country | [optional] 
**iso_country** | **str** | The ISO country code | [optional] 
**terminating_prefix_prices** | [**List[PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner]**](PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner.md) |  | [optional] 
**originating_call_price** | [**PricingV2TrunkingNumberOriginatingCallPrice**](PricingV2TrunkingNumberOriginatingCallPrice.md) |  | [optional] 
**price_unit** | **str** | The currency in which prices are measured, in ISO 4127 format (e.g. usd, eur, jpy) | [optional] 
**url** | **str** | The absolute URL of the resource | [optional] 

## Example

```python
from py_twilio_pricing.models.pricing_v2_trunking_number import PricingV2TrunkingNumber

# TODO update the JSON string below
json = "{}"
# create an instance of PricingV2TrunkingNumber from a JSON string
pricing_v2_trunking_number_instance = PricingV2TrunkingNumber.from_json(json)
# print the JSON string representation of the object
print PricingV2TrunkingNumber.to_json()

# convert the object into a dict
pricing_v2_trunking_number_dict = pricing_v2_trunking_number_instance.to_dict()
# create an instance of PricingV2TrunkingNumber from a dict
pricing_v2_trunking_number_form_dict = pricing_v2_trunking_number.from_dict(pricing_v2_trunking_number_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


