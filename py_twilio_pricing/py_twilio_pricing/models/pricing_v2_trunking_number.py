# coding: utf-8

"""
    Twilio - Pricing

    This is the public Twilio REST API.  # noqa: E501

    The version of the OpenAPI document: 1.37.3
    Contact: support@twilio.com
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json


from typing import List, Optional
from pydantic import BaseModel, Field, StrictStr
from py_twilio_pricing.models.pricing_v2_trunking_country_instance_terminating_prefix_prices_inner import PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner
from py_twilio_pricing.models.pricing_v2_trunking_number_originating_call_price import PricingV2TrunkingNumberOriginatingCallPrice

class PricingV2TrunkingNumber(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    destination_number: Optional[StrictStr] = Field(None, description="The destination phone number, in E.164 format")
    origination_number: Optional[StrictStr] = Field(None, description="The origination phone number, in E.164 format")
    country: Optional[StrictStr] = Field(None, description="The name of the country")
    iso_country: Optional[StrictStr] = Field(None, description="The ISO country code")
    terminating_prefix_prices: Optional[List[PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner]] = None
    originating_call_price: Optional[PricingV2TrunkingNumberOriginatingCallPrice] = None
    price_unit: Optional[StrictStr] = Field(None, description="The currency in which prices are measured, in ISO 4127 format (e.g. usd, eur, jpy)")
    url: Optional[StrictStr] = Field(None, description="The absolute URL of the resource")
    __properties = ["destination_number", "origination_number", "country", "iso_country", "terminating_prefix_prices", "originating_call_price", "price_unit", "url"]

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> PricingV2TrunkingNumber:
        """Create an instance of PricingV2TrunkingNumber from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True,
                          exclude={
                          },
                          exclude_none=True)
        # override the default output from pydantic by calling `to_dict()` of each item in terminating_prefix_prices (list)
        _items = []
        if self.terminating_prefix_prices:
            for _item in self.terminating_prefix_prices:
                if _item:
                    _items.append(_item.to_dict())
            _dict['terminating_prefix_prices'] = _items
        # override the default output from pydantic by calling `to_dict()` of originating_call_price
        if self.originating_call_price:
            _dict['originating_call_price'] = self.originating_call_price.to_dict()
        # set to None if destination_number (nullable) is None
        if self.destination_number is None:
            _dict['destination_number'] = None

        # set to None if origination_number (nullable) is None
        if self.origination_number is None:
            _dict['origination_number'] = None

        # set to None if country (nullable) is None
        if self.country is None:
            _dict['country'] = None

        # set to None if iso_country (nullable) is None
        if self.iso_country is None:
            _dict['iso_country'] = None

        # set to None if terminating_prefix_prices (nullable) is None
        if self.terminating_prefix_prices is None:
            _dict['terminating_prefix_prices'] = None

        # set to None if originating_call_price (nullable) is None
        if self.originating_call_price is None:
            _dict['originating_call_price'] = None

        # set to None if price_unit (nullable) is None
        if self.price_unit is None:
            _dict['price_unit'] = None

        # set to None if url (nullable) is None
        if self.url is None:
            _dict['url'] = None

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> PricingV2TrunkingNumber:
        """Create an instance of PricingV2TrunkingNumber from a dict"""
        if obj is None:
            return None

        if type(obj) is not dict:
            return PricingV2TrunkingNumber.parse_obj(obj)

        _obj = PricingV2TrunkingNumber.parse_obj({
            "destination_number": obj.get("destination_number"),
            "origination_number": obj.get("origination_number"),
            "country": obj.get("country"),
            "iso_country": obj.get("iso_country"),
            "terminating_prefix_prices": [PricingV2TrunkingCountryInstanceTerminatingPrefixPricesInner.from_dict(_item) for _item in obj.get("terminating_prefix_prices")] if obj.get("terminating_prefix_prices") is not None else None,
            "originating_call_price": PricingV2TrunkingNumberOriginatingCallPrice.from_dict(obj.get("originating_call_price")) if obj.get("originating_call_price") is not None else None,
            "price_unit": obj.get("price_unit"),
            "url": obj.get("url")
        })
        return _obj

