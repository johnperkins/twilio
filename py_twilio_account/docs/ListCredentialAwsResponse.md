# ListCredentialAwsResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**credentials** | [**List[AccountsV1CredentialCredentialAws]**](AccountsV1CredentialCredentialAws.md) |  | [optional] 
**meta** | [**ListCredentialAwsResponseMeta**](ListCredentialAwsResponseMeta.md) |  | [optional] 

## Example

```python
from py_twilio_account.models.list_credential_aws_response import ListCredentialAwsResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ListCredentialAwsResponse from a JSON string
list_credential_aws_response_instance = ListCredentialAwsResponse.from_json(json)
# print the JSON string representation of the object
print ListCredentialAwsResponse.to_json()

# convert the object into a dict
list_credential_aws_response_dict = list_credential_aws_response_instance.to_dict()
# create an instance of ListCredentialAwsResponse from a dict
list_credential_aws_response_form_dict = list_credential_aws_response.from_dict(list_credential_aws_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


