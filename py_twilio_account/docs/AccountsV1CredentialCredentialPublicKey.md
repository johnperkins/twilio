# AccountsV1CredentialCredentialPublicKey


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | The unique string that identifies the resource | [optional] 
**account_sid** | **str** | The SID of the Account that created the Credential that the PublicKey resource belongs to | [optional] 
**friendly_name** | **str** | The string that you assigned to describe the resource | [optional] 
**date_created** | **datetime** | The RFC 2822 date and time in GMT when the resource was created | [optional] 
**date_updated** | **datetime** | The RFC 2822 date and time in GMT when the resource was last updated | [optional] 
**url** | **str** | The URI for this resource, relative to &#x60;https://accounts.twilio.com&#x60; | [optional] 

## Example

```python
from py_twilio_account.models.accounts_v1_credential_credential_public_key import AccountsV1CredentialCredentialPublicKey

# TODO update the JSON string below
json = "{}"
# create an instance of AccountsV1CredentialCredentialPublicKey from a JSON string
accounts_v1_credential_credential_public_key_instance = AccountsV1CredentialCredentialPublicKey.from_json(json)
# print the JSON string representation of the object
print AccountsV1CredentialCredentialPublicKey.to_json()

# convert the object into a dict
accounts_v1_credential_credential_public_key_dict = accounts_v1_credential_credential_public_key_instance.to_dict()
# create an instance of AccountsV1CredentialCredentialPublicKey from a dict
accounts_v1_credential_credential_public_key_form_dict = accounts_v1_credential_credential_public_key.from_dict(accounts_v1_credential_credential_public_key_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


