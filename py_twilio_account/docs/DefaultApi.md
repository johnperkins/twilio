# py_twilio_account.DefaultApi

All URIs are relative to *https://accounts.twilio.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_credential_aws**](DefaultApi.md#create_credential_aws) | **POST** /v1/Credentials/AWS | 
[**create_credential_public_key**](DefaultApi.md#create_credential_public_key) | **POST** /v1/Credentials/PublicKeys | 
[**create_secondary_auth_token**](DefaultApi.md#create_secondary_auth_token) | **POST** /v1/AuthTokens/Secondary | 
[**delete_credential_aws**](DefaultApi.md#delete_credential_aws) | **DELETE** /v1/Credentials/AWS/{Sid} | 
[**delete_credential_public_key**](DefaultApi.md#delete_credential_public_key) | **DELETE** /v1/Credentials/PublicKeys/{Sid} | 
[**delete_secondary_auth_token**](DefaultApi.md#delete_secondary_auth_token) | **DELETE** /v1/AuthTokens/Secondary | 
[**fetch_credential_aws**](DefaultApi.md#fetch_credential_aws) | **GET** /v1/Credentials/AWS/{Sid} | 
[**fetch_credential_public_key**](DefaultApi.md#fetch_credential_public_key) | **GET** /v1/Credentials/PublicKeys/{Sid} | 
[**list_credential_aws**](DefaultApi.md#list_credential_aws) | **GET** /v1/Credentials/AWS | 
[**list_credential_public_key**](DefaultApi.md#list_credential_public_key) | **GET** /v1/Credentials/PublicKeys | 
[**update_auth_token_promotion**](DefaultApi.md#update_auth_token_promotion) | **POST** /v1/AuthTokens/Promote | 
[**update_credential_aws**](DefaultApi.md#update_credential_aws) | **POST** /v1/Credentials/AWS/{Sid} | 
[**update_credential_public_key**](DefaultApi.md#update_credential_public_key) | **POST** /v1/Credentials/PublicKeys/{Sid} | 


# **create_credential_aws**
> AccountsV1CredentialCredentialAws create_credential_aws(credentials, friendly_name=friendly_name, account_sid=account_sid)



Create a new AWS Credential

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)
    credentials = 'credentials_example' # str | A string that contains the AWS access credentials in the format `<AWS_ACCESS_KEY_ID>:<AWS_SECRET_ACCESS_KEY>`. For example, `AKIAIOSFODNN7EXAMPLE:wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY`
    friendly_name = 'friendly_name_example' # str | A descriptive string that you create to describe the resource. It can be up to 64 characters long. (optional)
    account_sid = 'account_sid_example' # str | The SID of the Subaccount that this Credential should be associated with. Must be a valid Subaccount of the account issuing the request. (optional)

    try:
        api_response = api_instance.create_credential_aws(credentials, friendly_name=friendly_name, account_sid=account_sid)
        print("The response of DefaultApi->create_credential_aws:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_credential_aws: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **credentials** | **str**| A string that contains the AWS access credentials in the format &#x60;&lt;AWS_ACCESS_KEY_ID&gt;:&lt;AWS_SECRET_ACCESS_KEY&gt;&#x60;. For example, &#x60;AKIAIOSFODNN7EXAMPLE:wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY&#x60; | 
 **friendly_name** | **str**| A descriptive string that you create to describe the resource. It can be up to 64 characters long. | [optional] 
 **account_sid** | **str**| The SID of the Subaccount that this Credential should be associated with. Must be a valid Subaccount of the account issuing the request. | [optional] 

### Return type

[**AccountsV1CredentialCredentialAws**](AccountsV1CredentialCredentialAws.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_credential_public_key**
> AccountsV1CredentialCredentialPublicKey create_credential_public_key(public_key, friendly_name=friendly_name, account_sid=account_sid)



Create a new Public Key Credential

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)
    public_key = 'public_key_example' # str | A URL encoded representation of the public key. For example, `-----BEGIN PUBLIC KEY-----MIIBIjANB.pa9xQIDAQAB-----END PUBLIC KEY-----`
    friendly_name = 'friendly_name_example' # str | A descriptive string that you create to describe the resource. It can be up to 64 characters long. (optional)
    account_sid = 'account_sid_example' # str | The SID of the Subaccount that this Credential should be associated with. Must be a valid Subaccount of the account issuing the request (optional)

    try:
        api_response = api_instance.create_credential_public_key(public_key, friendly_name=friendly_name, account_sid=account_sid)
        print("The response of DefaultApi->create_credential_public_key:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_credential_public_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **public_key** | **str**| A URL encoded representation of the public key. For example, &#x60;-----BEGIN PUBLIC KEY-----MIIBIjANB.pa9xQIDAQAB-----END PUBLIC KEY-----&#x60; | 
 **friendly_name** | **str**| A descriptive string that you create to describe the resource. It can be up to 64 characters long. | [optional] 
 **account_sid** | **str**| The SID of the Subaccount that this Credential should be associated with. Must be a valid Subaccount of the account issuing the request | [optional] 

### Return type

[**AccountsV1CredentialCredentialPublicKey**](AccountsV1CredentialCredentialPublicKey.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_secondary_auth_token**
> AccountsV1SecondaryAuthToken create_secondary_auth_token()



Create a new secondary Auth Token

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)

    try:
        api_response = api_instance.create_secondary_auth_token()
        print("The response of DefaultApi->create_secondary_auth_token:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_secondary_auth_token: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AccountsV1SecondaryAuthToken**](AccountsV1SecondaryAuthToken.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_credential_aws**
> delete_credential_aws(sid)



Delete a Credential from your account

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the AWS resource to delete.

    try:
        api_instance.delete_credential_aws(sid)
    except Exception as e:
        print("Exception when calling DefaultApi->delete_credential_aws: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sid** | **str**| The Twilio-provided string that uniquely identifies the AWS resource to delete. | 

### Return type

void (empty response body)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | The resource was deleted successfully. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_credential_public_key**
> delete_credential_public_key(sid)



Delete a Credential from your account

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the PublicKey resource to delete.

    try:
        api_instance.delete_credential_public_key(sid)
    except Exception as e:
        print("Exception when calling DefaultApi->delete_credential_public_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sid** | **str**| The Twilio-provided string that uniquely identifies the PublicKey resource to delete. | 

### Return type

void (empty response body)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | The resource was deleted successfully. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_secondary_auth_token**
> delete_secondary_auth_token()



Delete the secondary Auth Token from your account

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)

    try:
        api_instance.delete_secondary_auth_token()
    except Exception as e:
        print("Exception when calling DefaultApi->delete_secondary_auth_token: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | The resource was deleted successfully. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_credential_aws**
> AccountsV1CredentialCredentialAws fetch_credential_aws(sid)



Fetch the AWS credentials specified by the provided Credential Sid

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the AWS resource to fetch.

    try:
        api_response = api_instance.fetch_credential_aws(sid)
        print("The response of DefaultApi->fetch_credential_aws:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_credential_aws: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sid** | **str**| The Twilio-provided string that uniquely identifies the AWS resource to fetch. | 

### Return type

[**AccountsV1CredentialCredentialAws**](AccountsV1CredentialCredentialAws.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_credential_public_key**
> AccountsV1CredentialCredentialPublicKey fetch_credential_public_key(sid)



Fetch the public key specified by the provided Credential Sid

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the PublicKey resource to fetch.

    try:
        api_response = api_instance.fetch_credential_public_key(sid)
        print("The response of DefaultApi->fetch_credential_public_key:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_credential_public_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sid** | **str**| The Twilio-provided string that uniquely identifies the PublicKey resource to fetch. | 

### Return type

[**AccountsV1CredentialCredentialPublicKey**](AccountsV1CredentialCredentialPublicKey.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_credential_aws**
> ListCredentialAwsResponse list_credential_aws(page_size=page_size)



Retrieves a collection of AWS Credentials belonging to the account used to make the request

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_credential_aws(page_size=page_size)
        print("The response of DefaultApi->list_credential_aws:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_credential_aws: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListCredentialAwsResponse**](ListCredentialAwsResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_credential_public_key**
> ListCredentialPublicKeyResponse list_credential_public_key(page_size=page_size)



Retrieves a collection of Public Key Credentials belonging to the account used to make the request

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_credential_public_key(page_size=page_size)
        print("The response of DefaultApi->list_credential_public_key:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_credential_public_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListCredentialPublicKeyResponse**](ListCredentialPublicKeyResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_auth_token_promotion**
> AccountsV1AuthTokenPromotion update_auth_token_promotion()



Promote the secondary Auth Token to primary. After promoting the new token, all requests to Twilio using your old primary Auth Token will result in an error.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)

    try:
        api_response = api_instance.update_auth_token_promotion()
        print("The response of DefaultApi->update_auth_token_promotion:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->update_auth_token_promotion: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AccountsV1AuthTokenPromotion**](AccountsV1AuthTokenPromotion.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_credential_aws**
> AccountsV1CredentialCredentialAws update_credential_aws(sid, friendly_name=friendly_name)



Modify the properties of a given Account

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the AWS resource to update.
    friendly_name = 'friendly_name_example' # str | A descriptive string that you create to describe the resource. It can be up to 64 characters long. (optional)

    try:
        api_response = api_instance.update_credential_aws(sid, friendly_name=friendly_name)
        print("The response of DefaultApi->update_credential_aws:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->update_credential_aws: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sid** | **str**| The Twilio-provided string that uniquely identifies the AWS resource to update. | 
 **friendly_name** | **str**| A descriptive string that you create to describe the resource. It can be up to 64 characters long. | [optional] 

### Return type

[**AccountsV1CredentialCredentialAws**](AccountsV1CredentialCredentialAws.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_credential_public_key**
> AccountsV1CredentialCredentialPublicKey update_credential_public_key(sid, friendly_name=friendly_name)



Modify the properties of a given Account

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_account
from py_twilio_account.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://accounts.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_account.Configuration(
    host = "https://accounts.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_account.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_account.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_account.DefaultApi(api_client)
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the PublicKey resource to update.
    friendly_name = 'friendly_name_example' # str | A descriptive string that you create to describe the resource. It can be up to 64 characters long. (optional)

    try:
        api_response = api_instance.update_credential_public_key(sid, friendly_name=friendly_name)
        print("The response of DefaultApi->update_credential_public_key:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->update_credential_public_key: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sid** | **str**| The Twilio-provided string that uniquely identifies the PublicKey resource to update. | 
 **friendly_name** | **str**| A descriptive string that you create to describe the resource. It can be up to 64 characters long. | [optional] 

### Return type

[**AccountsV1CredentialCredentialPublicKey**](AccountsV1CredentialCredentialPublicKey.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

