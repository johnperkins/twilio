# AccountsV1AuthTokenPromotion


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str** | The SID of the Account that the secondary Auth Token was created for | [optional] 
**auth_token** | **str** | The promoted Auth Token | [optional] 
**date_created** | **datetime** | The ISO 8601 formatted date and time in UTC when the resource was created | [optional] 
**date_updated** | **datetime** | The ISO 8601 formatted date and time in UTC when the resource was last updated | [optional] 
**url** | **str** | The URI for this resource, relative to &#x60;https://accounts.twilio.com&#x60; | [optional] 

## Example

```python
from py_twilio_account.models.accounts_v1_auth_token_promotion import AccountsV1AuthTokenPromotion

# TODO update the JSON string below
json = "{}"
# create an instance of AccountsV1AuthTokenPromotion from a JSON string
accounts_v1_auth_token_promotion_instance = AccountsV1AuthTokenPromotion.from_json(json)
# print the JSON string representation of the object
print AccountsV1AuthTokenPromotion.to_json()

# convert the object into a dict
accounts_v1_auth_token_promotion_dict = accounts_v1_auth_token_promotion_instance.to_dict()
# create an instance of AccountsV1AuthTokenPromotion from a dict
accounts_v1_auth_token_promotion_form_dict = accounts_v1_auth_token_promotion.from_dict(accounts_v1_auth_token_promotion_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


