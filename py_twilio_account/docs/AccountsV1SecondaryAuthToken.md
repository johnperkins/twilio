# AccountsV1SecondaryAuthToken


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str** | The SID of the Account that the secondary Auth Token was created for | [optional] 
**date_created** | **datetime** | The ISO 8601 formatted date and time in UTC when the resource was created | [optional] 
**date_updated** | **datetime** | The ISO 8601 formatted date and time in UTC when the resource was last updated | [optional] 
**secondary_auth_token** | **str** | The generated secondary Auth Token | [optional] 
**url** | **str** | The URI for this resource, relative to &#x60;https://accounts.twilio.com&#x60; | [optional] 

## Example

```python
from py_twilio_account.models.accounts_v1_secondary_auth_token import AccountsV1SecondaryAuthToken

# TODO update the JSON string below
json = "{}"
# create an instance of AccountsV1SecondaryAuthToken from a JSON string
accounts_v1_secondary_auth_token_instance = AccountsV1SecondaryAuthToken.from_json(json)
# print the JSON string representation of the object
print AccountsV1SecondaryAuthToken.to_json()

# convert the object into a dict
accounts_v1_secondary_auth_token_dict = accounts_v1_secondary_auth_token_instance.to_dict()
# create an instance of AccountsV1SecondaryAuthToken from a dict
accounts_v1_secondary_auth_token_form_dict = accounts_v1_secondary_auth_token.from_dict(accounts_v1_secondary_auth_token_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


