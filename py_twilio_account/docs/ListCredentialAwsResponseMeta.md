# ListCredentialAwsResponseMeta


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**first_page_url** | **str** |  | [optional] 
**next_page_url** | **str** |  | [optional] 
**page** | **int** |  | [optional] 
**page_size** | **int** |  | [optional] 
**previous_page_url** | **str** |  | [optional] 
**url** | **str** |  | [optional] 
**key** | **str** |  | [optional] 

## Example

```python
from py_twilio_account.models.list_credential_aws_response_meta import ListCredentialAwsResponseMeta

# TODO update the JSON string below
json = "{}"
# create an instance of ListCredentialAwsResponseMeta from a JSON string
list_credential_aws_response_meta_instance = ListCredentialAwsResponseMeta.from_json(json)
# print the JSON string representation of the object
print ListCredentialAwsResponseMeta.to_json()

# convert the object into a dict
list_credential_aws_response_meta_dict = list_credential_aws_response_meta_instance.to_dict()
# create an instance of ListCredentialAwsResponseMeta from a dict
list_credential_aws_response_meta_form_dict = list_credential_aws_response_meta.from_dict(list_credential_aws_response_meta_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


