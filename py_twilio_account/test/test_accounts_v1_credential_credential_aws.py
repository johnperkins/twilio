# coding: utf-8

"""
    Twilio - Accounts

    This is the public Twilio REST API.  # noqa: E501

    The version of the OpenAPI document: 1.37.3
    Contact: support@twilio.com
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import py_twilio_account
from py_twilio_account.models.accounts_v1_credential_credential_aws import AccountsV1CredentialCredentialAws  # noqa: E501
from py_twilio_account.rest import ApiException

class TestAccountsV1CredentialCredentialAws(unittest.TestCase):
    """AccountsV1CredentialCredentialAws unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test AccountsV1CredentialCredentialAws
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `AccountsV1CredentialCredentialAws`
        """
        model = py_twilio_account.models.accounts_v1_credential_credential_aws.AccountsV1CredentialCredentialAws()  # noqa: E501
        if include_optional :
            return AccountsV1CredentialCredentialAws(
                sid = 'CR62ECB020842930cc01FFCCfeEe150AC30123456789101112131415161718192021222324252627282930313233', 
                account_sid = 'AC62ECB020842930cc01FFCCfeEe150AC30123456789101112131415161718192021222324252627282930313233', 
                friendly_name = '', 
                date_created = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                date_updated = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                url = ''
            )
        else :
            return AccountsV1CredentialCredentialAws(
        )
        """

    def testAccountsV1CredentialCredentialAws(self):
        """Test AccountsV1CredentialCredentialAws"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
