# coding: utf-8

"""
    Twilio - Accounts

    This is the public Twilio REST API.  # noqa: E501

    The version of the OpenAPI document: 1.37.3
    Contact: support@twilio.com
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import datetime
from typing import Optional
from pydantic import BaseModel, Field, StrictStr, constr, validator

class AccountsV1SecondaryAuthToken(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    account_sid: Optional[constr(strict=True, max_length=34, min_length=34)] = Field(None, description="The SID of the Account that the secondary Auth Token was created for")
    date_created: Optional[datetime] = Field(None, description="The ISO 8601 formatted date and time in UTC when the resource was created")
    date_updated: Optional[datetime] = Field(None, description="The ISO 8601 formatted date and time in UTC when the resource was last updated")
    secondary_auth_token: Optional[StrictStr] = Field(None, description="The generated secondary Auth Token")
    url: Optional[StrictStr] = Field(None, description="The URI for this resource, relative to `https://accounts.twilio.com`")
    __properties = ["account_sid", "date_created", "date_updated", "secondary_auth_token", "url"]

    @validator('account_sid')
    def account_sid_validate_regular_expression(cls, v):
        if not re.match(r"^AC[0-9a-fA-F]{32}$", v):
            raise ValueError(r"must validate the regular expression /^AC[0-9a-fA-F]{32}$/")
        return v

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> AccountsV1SecondaryAuthToken:
        """Create an instance of AccountsV1SecondaryAuthToken from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True,
                          exclude={
                          },
                          exclude_none=True)
        # set to None if account_sid (nullable) is None
        if self.account_sid is None:
            _dict['account_sid'] = None

        # set to None if date_created (nullable) is None
        if self.date_created is None:
            _dict['date_created'] = None

        # set to None if date_updated (nullable) is None
        if self.date_updated is None:
            _dict['date_updated'] = None

        # set to None if secondary_auth_token (nullable) is None
        if self.secondary_auth_token is None:
            _dict['secondary_auth_token'] = None

        # set to None if url (nullable) is None
        if self.url is None:
            _dict['url'] = None

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> AccountsV1SecondaryAuthToken:
        """Create an instance of AccountsV1SecondaryAuthToken from a dict"""
        if obj is None:
            return None

        if type(obj) is not dict:
            return AccountsV1SecondaryAuthToken.parse_obj(obj)

        _obj = AccountsV1SecondaryAuthToken.parse_obj({
            "account_sid": obj.get("account_sid"),
            "date_created": obj.get("date_created"),
            "date_updated": obj.get("date_updated"),
            "secondary_auth_token": obj.get("secondary_auth_token"),
            "url": obj.get("url")
        })
        return _obj

