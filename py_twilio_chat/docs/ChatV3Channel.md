# ChatV3Channel


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | The unique string that identifies the resource | [optional] 
**account_sid** | **str** | The SID of the Account that created the resource | [optional] 
**service_sid** | **str** | The SID of the Service that the resource is associated with | [optional] 
**friendly_name** | **str** | The string that you assigned to describe the resource | [optional] 
**unique_name** | **str** | An application-defined string that uniquely identifies the resource | [optional] 
**attributes** | **str** | The JSON string that stores application-specific data | [optional] 
**type** | [**ChannelEnumChannelType**](ChannelEnumChannelType.md) |  | [optional] 
**date_created** | **datetime** | The ISO 8601 date and time in GMT when the resource was created | [optional] 
**date_updated** | **datetime** | The ISO 8601 date and time in GMT when the resource was last updated | [optional] 
**created_by** | **str** | The identity of the User that created the channel | [optional] 
**members_count** | **int** | The number of Members in the Channel | [optional] 
**messages_count** | **int** | The number of Messages that have been passed in the Channel | [optional] 
**messaging_service_sid** | **str** | The unique ID of the Messaging Service this channel belongs to. | [optional] 
**url** | **str** | The absolute URL of the Channel resource | [optional] 

## Example

```python
from py_twilio_chat.models.chat_v3_channel import ChatV3Channel

# TODO update the JSON string below
json = "{}"
# create an instance of ChatV3Channel from a JSON string
chat_v3_channel_instance = ChatV3Channel.from_json(json)
# print the JSON string representation of the object
print ChatV3Channel.to_json()

# convert the object into a dict
chat_v3_channel_dict = chat_v3_channel_instance.to_dict()
# create an instance of ChatV3Channel from a dict
chat_v3_channel_form_dict = chat_v3_channel.from_dict(chat_v3_channel_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


