# py_twilio_chat.DefaultApi

All URIs are relative to *https://chat.twilio.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**update_channel**](DefaultApi.md#update_channel) | **POST** /v3/Services/{ServiceSid}/Channels/{Sid} | 


# **update_channel**
> ChatV3Channel update_channel(service_sid, sid, x_twilio_webhook_enabled=x_twilio_webhook_enabled, type=type, messaging_service_sid=messaging_service_sid)



Update a specific Channel.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_chat
from py_twilio_chat.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://chat.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_chat.Configuration(
    host = "https://chat.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_chat.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_chat.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_chat.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    sid = 'sid_example' # str | A 34 character string that uniquely identifies this Channel.
    x_twilio_webhook_enabled = py_twilio_chat.ChannelEnumWebhookEnabledType() # ChannelEnumWebhookEnabledType | The X-Twilio-Webhook-Enabled HTTP request header (optional)
    type = py_twilio_chat.ChannelEnumChannelType() # ChannelEnumChannelType |  (optional)
    messaging_service_sid = 'messaging_service_sid_example' # str | The unique ID of the [Messaging Service](https://www.twilio.com/docs/sms/services/api) this channel belongs to. (optional)

    try:
        api_response = api_instance.update_channel(service_sid, sid, x_twilio_webhook_enabled=x_twilio_webhook_enabled, type=type, messaging_service_sid=messaging_service_sid)
        print("The response of DefaultApi->update_channel:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->update_channel: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **sid** | **str**| A 34 character string that uniquely identifies this Channel. | 
 **x_twilio_webhook_enabled** | **ChannelEnumWebhookEnabledType**| The X-Twilio-Webhook-Enabled HTTP request header | [optional] 
 **type** | **ChannelEnumChannelType**|  | [optional] 
 **messaging_service_sid** | **str**| The unique ID of the [Messaging Service](https://www.twilio.com/docs/sms/services/api) this channel belongs to. | [optional] 

### Return type

[**ChatV3Channel**](ChatV3Channel.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

