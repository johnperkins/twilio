# coding: utf-8

"""
    Twilio - Verify

    This is the public Twilio REST API.  # noqa: E501

    The version of the OpenAPI document: 1.37.3
    Contact: support@twilio.com
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import py_twilio_verify
from py_twilio_verify.models.verification_attempt_enum_message_status import VerificationAttemptEnumMessageStatus  # noqa: E501
from py_twilio_verify.rest import ApiException

class TestVerificationAttemptEnumMessageStatus(unittest.TestCase):
    """VerificationAttemptEnumMessageStatus unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testVerificationAttemptEnumMessageStatus(self):
        """Test VerificationAttemptEnumMessageStatus"""
        # inst = VerificationAttemptEnumMessageStatus()

if __name__ == '__main__':
    unittest.main()
