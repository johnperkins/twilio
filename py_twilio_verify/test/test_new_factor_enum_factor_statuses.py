# coding: utf-8

"""
    Twilio - Verify

    This is the public Twilio REST API.  # noqa: E501

    The version of the OpenAPI document: 1.37.3
    Contact: support@twilio.com
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import py_twilio_verify
from py_twilio_verify.models.new_factor_enum_factor_statuses import NewFactorEnumFactorStatuses  # noqa: E501
from py_twilio_verify.rest import ApiException

class TestNewFactorEnumFactorStatuses(unittest.TestCase):
    """NewFactorEnumFactorStatuses unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testNewFactorEnumFactorStatuses(self):
        """Test NewFactorEnumFactorStatuses"""
        # inst = NewFactorEnumFactorStatuses()

if __name__ == '__main__':
    unittest.main()
