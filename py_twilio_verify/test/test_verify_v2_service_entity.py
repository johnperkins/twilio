# coding: utf-8

"""
    Twilio - Verify

    This is the public Twilio REST API.  # noqa: E501

    The version of the OpenAPI document: 1.37.3
    Contact: support@twilio.com
    Generated by: https://openapi-generator.tech
"""


from __future__ import absolute_import

import unittest
import datetime

import py_twilio_verify
from py_twilio_verify.models.verify_v2_service_entity import VerifyV2ServiceEntity  # noqa: E501
from py_twilio_verify.rest import ApiException

class TestVerifyV2ServiceEntity(unittest.TestCase):
    """VerifyV2ServiceEntity unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def make_instance(self, include_optional):
        """Test VerifyV2ServiceEntity
            include_option is a boolean, when False only required
            params are included, when True both required and
            optional params are included """
        # uncomment below to create an instance of `VerifyV2ServiceEntity`
        """
        model = py_twilio_verify.models.verify_v2_service_entity.VerifyV2ServiceEntity()  # noqa: E501
        if include_optional :
            return VerifyV2ServiceEntity(
                sid = 'YE62ECB020842930cc01FFCCfeEe150AC30123456789101112131415161718192021222324252627282930313233', 
                identity = '', 
                account_sid = 'AC62ECB020842930cc01FFCCfeEe150AC30123456789101112131415161718192021222324252627282930313233', 
                service_sid = 'VA62ECB020842930cc01FFCCfeEe150AC30123456789101112131415161718192021222324252627282930313233', 
                date_created = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                date_updated = datetime.datetime.strptime('2013-10-20 19:20:30.00', '%Y-%m-%d %H:%M:%S.%f'), 
                url = '', 
                links = None
            )
        else :
            return VerifyV2ServiceEntity(
        )
        """

    def testVerifyV2ServiceEntity(self):
        """Test VerifyV2ServiceEntity"""
        # inst_req_only = self.make_instance(include_optional=False)
        # inst_req_and_optional = self.make_instance(include_optional=True)

if __name__ == '__main__':
    unittest.main()
