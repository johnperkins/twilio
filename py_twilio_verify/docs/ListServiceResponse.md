# ListServiceResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**services** | [**List[VerifyV2Service]**](VerifyV2Service.md) |  | [optional] 
**meta** | [**ListBucketResponseMeta**](ListBucketResponseMeta.md) |  | [optional] 

## Example

```python
from py_twilio_verify.models.list_service_response import ListServiceResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ListServiceResponse from a JSON string
list_service_response_instance = ListServiceResponse.from_json(json)
# print the JSON string representation of the object
print ListServiceResponse.to_json()

# convert the object into a dict
list_service_response_dict = list_service_response_instance.to_dict()
# create an instance of ListServiceResponse from a dict
list_service_response_form_dict = list_service_response.from_dict(list_service_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


