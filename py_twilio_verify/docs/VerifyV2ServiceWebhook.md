# VerifyV2ServiceWebhook


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | The unique string that identifies the resource | [optional] 
**service_sid** | **str** | Service Sid. | [optional] 
**account_sid** | **str** | The SID of the Account that created the resource | [optional] 
**friendly_name** | **str** | The string that you assigned to describe the webhook | [optional] 
**event_types** | **List[str]** | The array of events that this Webhook is subscribed to. | [optional] 
**status** | [**WebhookEnumStatus**](WebhookEnumStatus.md) |  | [optional] 
**version** | [**WebhookEnumVersion**](WebhookEnumVersion.md) |  | [optional] 
**webhook_url** | **str** | The URL associated with this Webhook. | [optional] 
**webhook_method** | [**WebhookEnumMethods**](WebhookEnumMethods.md) |  | [optional] 
**date_created** | **datetime** | The ISO 8601 date and time in GMT when the resource was created | [optional] 
**date_updated** | **datetime** | The ISO 8601 date and time in GMT when the resource was last updated | [optional] 
**url** | **str** | The absolute URL of the Webhook resource | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_service_webhook import VerifyV2ServiceWebhook

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2ServiceWebhook from a JSON string
verify_v2_service_webhook_instance = VerifyV2ServiceWebhook.from_json(json)
# print the JSON string representation of the object
print VerifyV2ServiceWebhook.to_json()

# convert the object into a dict
verify_v2_service_webhook_dict = verify_v2_service_webhook_instance.to_dict()
# create an instance of VerifyV2ServiceWebhook from a dict
verify_v2_service_webhook_form_dict = verify_v2_service_webhook.from_dict(verify_v2_service_webhook_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


