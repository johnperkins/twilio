# VerifyV2ServiceRateLimit


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | A string that uniquely identifies this Rate Limit. | [optional] 
**service_sid** | **str** | The SID of the Service that the resource is associated with | [optional] 
**account_sid** | **str** | The SID of the Account that created the resource | [optional] 
**unique_name** | **str** | A unique, developer assigned name of this Rate Limit. | [optional] 
**description** | **str** | Description of this Rate Limit | [optional] 
**date_created** | **datetime** | The RFC 2822 date and time in GMT when the resource was created | [optional] 
**date_updated** | **datetime** | The RFC 2822 date and time in GMT when the resource was last updated | [optional] 
**url** | **str** | The URL of this resource. | [optional] 
**links** | **object** | The URLs of related resources | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_service_rate_limit import VerifyV2ServiceRateLimit

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2ServiceRateLimit from a JSON string
verify_v2_service_rate_limit_instance = VerifyV2ServiceRateLimit.from_json(json)
# print the JSON string representation of the object
print VerifyV2ServiceRateLimit.to_json()

# convert the object into a dict
verify_v2_service_rate_limit_dict = verify_v2_service_rate_limit_instance.to_dict()
# create an instance of VerifyV2ServiceRateLimit from a dict
verify_v2_service_rate_limit_form_dict = verify_v2_service_rate_limit.from_dict(verify_v2_service_rate_limit_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


