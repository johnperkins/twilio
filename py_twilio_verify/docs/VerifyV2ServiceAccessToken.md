# VerifyV2ServiceAccessToken


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | A string that uniquely identifies this Access Token. | [optional] 
**account_sid** | **str** | Account Sid. | [optional] 
**service_sid** | **str** | Verify Service Sid. | [optional] 
**entity_identity** | **str** | Unique external identifier of the Entity | [optional] 
**factor_type** | [**AccessTokenEnumFactorTypes**](AccessTokenEnumFactorTypes.md) |  | [optional] 
**factor_friendly_name** | **str** | A human readable description of this factor. | [optional] 
**token** | **str** | Generated access token. | [optional] 
**url** | **str** | The URL of this resource. | [optional] 
**ttl** | **int** | How long, in seconds, the access token is valid. | [optional] 
**date_created** | **datetime** | The date this access token was created | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_service_access_token import VerifyV2ServiceAccessToken

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2ServiceAccessToken from a JSON string
verify_v2_service_access_token_instance = VerifyV2ServiceAccessToken.from_json(json)
# print the JSON string representation of the object
print VerifyV2ServiceAccessToken.to_json()

# convert the object into a dict
verify_v2_service_access_token_dict = verify_v2_service_access_token_instance.to_dict()
# create an instance of VerifyV2ServiceAccessToken from a dict
verify_v2_service_access_token_form_dict = verify_v2_service_access_token.from_dict(verify_v2_service_access_token_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


