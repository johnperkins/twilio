# VerifyV2ServiceEntityChallenge


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | A string that uniquely identifies this Challenge. | [optional] 
**account_sid** | **str** | Account Sid. | [optional] 
**service_sid** | **str** | Service Sid. | [optional] 
**entity_sid** | **str** | Entity Sid. | [optional] 
**identity** | **str** | Unique external identifier of the Entity | [optional] 
**factor_sid** | **str** | Factor Sid. | [optional] 
**date_created** | **datetime** | The date this Challenge was created | [optional] 
**date_updated** | **datetime** | The date this Challenge was updated | [optional] 
**date_responded** | **datetime** | The date this Challenge was responded | [optional] 
**expiration_date** | **datetime** | The date-time when this Challenge expires | [optional] 
**status** | [**ChallengeEnumChallengeStatuses**](ChallengeEnumChallengeStatuses.md) |  | [optional] 
**responded_reason** | [**ChallengeEnumChallengeReasons**](ChallengeEnumChallengeReasons.md) |  | [optional] 
**details** | **object** | Details about the Challenge. | [optional] 
**hidden_details** | **object** | Hidden details about the Challenge | [optional] 
**metadata** | **object** | Metadata of the challenge. | [optional] 
**factor_type** | [**ChallengeEnumFactorTypes**](ChallengeEnumFactorTypes.md) |  | [optional] 
**url** | **str** | The URL of this resource. | [optional] 
**links** | **object** | Nested resource URLs. | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_service_entity_challenge import VerifyV2ServiceEntityChallenge

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2ServiceEntityChallenge from a JSON string
verify_v2_service_entity_challenge_instance = VerifyV2ServiceEntityChallenge.from_json(json)
# print the JSON string representation of the object
print VerifyV2ServiceEntityChallenge.to_json()

# convert the object into a dict
verify_v2_service_entity_challenge_dict = verify_v2_service_entity_challenge_instance.to_dict()
# create an instance of VerifyV2ServiceEntityChallenge from a dict
verify_v2_service_entity_challenge_form_dict = verify_v2_service_entity_challenge.from_dict(verify_v2_service_entity_challenge_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


