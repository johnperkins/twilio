# VerifyV2ServiceRateLimitBucket


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | A string that uniquely identifies this Bucket. | [optional] 
**rate_limit_sid** | **str** | Rate Limit Sid. | [optional] 
**service_sid** | **str** | The SID of the Service that the resource is associated with | [optional] 
**account_sid** | **str** | The SID of the Account that created the resource | [optional] 
**max** | **int** | Max number of requests. | [optional] 
**interval** | **int** | Number of seconds that the rate limit will be enforced over. | [optional] 
**date_created** | **datetime** | The RFC 2822 date and time in GMT when the resource was created | [optional] 
**date_updated** | **datetime** | The RFC 2822 date and time in GMT when the resource was last updated | [optional] 
**url** | **str** | The URL of this resource. | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_service_rate_limit_bucket import VerifyV2ServiceRateLimitBucket

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2ServiceRateLimitBucket from a JSON string
verify_v2_service_rate_limit_bucket_instance = VerifyV2ServiceRateLimitBucket.from_json(json)
# print the JSON string representation of the object
print VerifyV2ServiceRateLimitBucket.to_json()

# convert the object into a dict
verify_v2_service_rate_limit_bucket_dict = verify_v2_service_rate_limit_bucket_instance.to_dict()
# create an instance of VerifyV2ServiceRateLimitBucket from a dict
verify_v2_service_rate_limit_bucket_form_dict = verify_v2_service_rate_limit_bucket.from_dict(verify_v2_service_rate_limit_bucket_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


