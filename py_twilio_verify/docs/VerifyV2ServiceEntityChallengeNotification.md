# VerifyV2ServiceEntityChallengeNotification


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | A string that uniquely identifies this Notification. | [optional] 
**account_sid** | **str** | Account Sid. | [optional] 
**service_sid** | **str** | Service Sid. | [optional] 
**entity_sid** | **str** | Entity Sid. | [optional] 
**identity** | **str** | Unique external identifier of the Entity | [optional] 
**challenge_sid** | **str** | Challenge Sid. | [optional] 
**priority** | **str** | The priority of the notification. | [optional] 
**ttl** | **int** | How long, in seconds, the notification is valid. | [optional] 
**date_created** | **datetime** | The date this Notification was created | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_service_entity_challenge_notification import VerifyV2ServiceEntityChallengeNotification

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2ServiceEntityChallengeNotification from a JSON string
verify_v2_service_entity_challenge_notification_instance = VerifyV2ServiceEntityChallengeNotification.from_json(json)
# print the JSON string representation of the object
print VerifyV2ServiceEntityChallengeNotification.to_json()

# convert the object into a dict
verify_v2_service_entity_challenge_notification_dict = verify_v2_service_entity_challenge_notification_instance.to_dict()
# create an instance of VerifyV2ServiceEntityChallengeNotification from a dict
verify_v2_service_entity_challenge_notification_form_dict = verify_v2_service_entity_challenge_notification.from_dict(verify_v2_service_entity_challenge_notification_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


