# VerifyV2VerificationTemplate


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | A string that uniquely identifies this Template | [optional] 
**account_sid** | **str** | Account Sid | [optional] 
**friendly_name** | **str** | A string to describe the verification template | [optional] 
**channels** | **List[str]** | A list of channels that support the Template | [optional] 
**translations** | **object** | Object with the template translations. | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_verification_template import VerifyV2VerificationTemplate

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2VerificationTemplate from a JSON string
verify_v2_verification_template_instance = VerifyV2VerificationTemplate.from_json(json)
# print the JSON string representation of the object
print VerifyV2VerificationTemplate.to_json()

# convert the object into a dict
verify_v2_verification_template_dict = verify_v2_verification_template_instance.to_dict()
# create an instance of VerifyV2VerificationTemplate from a dict
verify_v2_verification_template_form_dict = verify_v2_verification_template.from_dict(verify_v2_verification_template_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


