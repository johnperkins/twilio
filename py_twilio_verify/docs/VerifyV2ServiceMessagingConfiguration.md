# VerifyV2ServiceMessagingConfiguration


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_sid** | **str** | The SID of the Account that created the resource | [optional] 
**service_sid** | **str** | The SID of the Service that the resource is associated with | [optional] 
**country** | **str** | The ISO-3166-1 country code of the country or &#x60;all&#x60;. | [optional] 
**messaging_service_sid** | **str** | The SID of the Messaging Service used for this configuration. | [optional] 
**date_created** | **datetime** | The RFC 2822 date and time in GMT when the resource was created | [optional] 
**date_updated** | **datetime** | The RFC 2822 date and time in GMT when the resource was last updated | [optional] 
**url** | **str** | The URL of this resource. | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_service_messaging_configuration import VerifyV2ServiceMessagingConfiguration

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2ServiceMessagingConfiguration from a JSON string
verify_v2_service_messaging_configuration_instance = VerifyV2ServiceMessagingConfiguration.from_json(json)
# print the JSON string representation of the object
print VerifyV2ServiceMessagingConfiguration.to_json()

# convert the object into a dict
verify_v2_service_messaging_configuration_dict = verify_v2_service_messaging_configuration_instance.to_dict()
# create an instance of VerifyV2ServiceMessagingConfiguration from a dict
verify_v2_service_messaging_configuration_form_dict = verify_v2_service_messaging_configuration.from_dict(verify_v2_service_messaging_configuration_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


