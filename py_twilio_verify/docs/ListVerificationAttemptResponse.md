# ListVerificationAttemptResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attempts** | [**List[VerifyV2VerificationAttempt]**](VerifyV2VerificationAttempt.md) |  | [optional] 
**meta** | [**ListBucketResponseMeta**](ListBucketResponseMeta.md) |  | [optional] 

## Example

```python
from py_twilio_verify.models.list_verification_attempt_response import ListVerificationAttemptResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ListVerificationAttemptResponse from a JSON string
list_verification_attempt_response_instance = ListVerificationAttemptResponse.from_json(json)
# print the JSON string representation of the object
print ListVerificationAttemptResponse.to_json()

# convert the object into a dict
list_verification_attempt_response_dict = list_verification_attempt_response_instance.to_dict()
# create an instance of ListVerificationAttemptResponse from a dict
list_verification_attempt_response_form_dict = list_verification_attempt_response.from_dict(list_verification_attempt_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


