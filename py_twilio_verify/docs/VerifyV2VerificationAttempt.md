# VerifyV2VerificationAttempt


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | The SID that uniquely identifies the verification attempt. | [optional] 
**account_sid** | **str** | The SID of the Account that created the verification. | [optional] 
**service_sid** | **str** | The SID of the verify service that generated this attempt. | [optional] 
**verification_sid** | **str** | The SID of the verification that generated this attempt. | [optional] 
**date_created** | **datetime** | The date this Attempt was created | [optional] 
**date_updated** | **datetime** | The date this Attempt was updated | [optional] 
**conversion_status** | [**VerificationAttemptEnumConversionStatus**](VerificationAttemptEnumConversionStatus.md) |  | [optional] 
**channel** | [**VerificationAttemptEnumChannels**](VerificationAttemptEnumChannels.md) |  | [optional] 
**price** | **object** | An object containing the charge for this verification attempt. | [optional] 
**channel_data** | **object** | An object containing the channel specific information for an attempt. | [optional] 
**url** | **str** |  | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_verification_attempt import VerifyV2VerificationAttempt

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2VerificationAttempt from a JSON string
verify_v2_verification_attempt_instance = VerifyV2VerificationAttempt.from_json(json)
# print the JSON string representation of the object
print VerifyV2VerificationAttempt.to_json()

# convert the object into a dict
verify_v2_verification_attempt_dict = verify_v2_verification_attempt_instance.to_dict()
# create an instance of VerifyV2VerificationAttempt from a dict
verify_v2_verification_attempt_form_dict = verify_v2_verification_attempt.from_dict(verify_v2_verification_attempt_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


