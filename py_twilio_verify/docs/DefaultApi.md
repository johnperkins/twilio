# py_twilio_verify.DefaultApi

All URIs are relative to *https://verify.twilio.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**create_access_token**](DefaultApi.md#create_access_token) | **POST** /v2/Services/{ServiceSid}/AccessTokens | 
[**create_bucket**](DefaultApi.md#create_bucket) | **POST** /v2/Services/{ServiceSid}/RateLimits/{RateLimitSid}/Buckets | 
[**create_challenge**](DefaultApi.md#create_challenge) | **POST** /v2/Services/{ServiceSid}/Entities/{Identity}/Challenges | 
[**create_entity**](DefaultApi.md#create_entity) | **POST** /v2/Services/{ServiceSid}/Entities | 
[**create_messaging_configuration**](DefaultApi.md#create_messaging_configuration) | **POST** /v2/Services/{ServiceSid}/MessagingConfigurations | 
[**create_new_factor**](DefaultApi.md#create_new_factor) | **POST** /v2/Services/{ServiceSid}/Entities/{Identity}/Factors | 
[**create_notification**](DefaultApi.md#create_notification) | **POST** /v2/Services/{ServiceSid}/Entities/{Identity}/Challenges/{ChallengeSid}/Notifications | 
[**create_rate_limit**](DefaultApi.md#create_rate_limit) | **POST** /v2/Services/{ServiceSid}/RateLimits | 
[**create_safelist**](DefaultApi.md#create_safelist) | **POST** /v2/SafeList/Numbers | 
[**create_service**](DefaultApi.md#create_service) | **POST** /v2/Services | 
[**create_verification**](DefaultApi.md#create_verification) | **POST** /v2/Services/{ServiceSid}/Verifications | 
[**create_verification_check**](DefaultApi.md#create_verification_check) | **POST** /v2/Services/{ServiceSid}/VerificationCheck | 
[**create_webhook**](DefaultApi.md#create_webhook) | **POST** /v2/Services/{ServiceSid}/Webhooks | 
[**delete_bucket**](DefaultApi.md#delete_bucket) | **DELETE** /v2/Services/{ServiceSid}/RateLimits/{RateLimitSid}/Buckets/{Sid} | 
[**delete_entity**](DefaultApi.md#delete_entity) | **DELETE** /v2/Services/{ServiceSid}/Entities/{Identity} | 
[**delete_factor**](DefaultApi.md#delete_factor) | **DELETE** /v2/Services/{ServiceSid}/Entities/{Identity}/Factors/{Sid} | 
[**delete_messaging_configuration**](DefaultApi.md#delete_messaging_configuration) | **DELETE** /v2/Services/{ServiceSid}/MessagingConfigurations/{Country} | 
[**delete_rate_limit**](DefaultApi.md#delete_rate_limit) | **DELETE** /v2/Services/{ServiceSid}/RateLimits/{Sid} | 
[**delete_safelist**](DefaultApi.md#delete_safelist) | **DELETE** /v2/SafeList/Numbers/{PhoneNumber} | 
[**delete_service**](DefaultApi.md#delete_service) | **DELETE** /v2/Services/{Sid} | 
[**delete_webhook**](DefaultApi.md#delete_webhook) | **DELETE** /v2/Services/{ServiceSid}/Webhooks/{Sid} | 
[**fetch_access_token**](DefaultApi.md#fetch_access_token) | **GET** /v2/Services/{ServiceSid}/AccessTokens/{Sid} | 
[**fetch_bucket**](DefaultApi.md#fetch_bucket) | **GET** /v2/Services/{ServiceSid}/RateLimits/{RateLimitSid}/Buckets/{Sid} | 
[**fetch_challenge**](DefaultApi.md#fetch_challenge) | **GET** /v2/Services/{ServiceSid}/Entities/{Identity}/Challenges/{Sid} | 
[**fetch_entity**](DefaultApi.md#fetch_entity) | **GET** /v2/Services/{ServiceSid}/Entities/{Identity} | 
[**fetch_factor**](DefaultApi.md#fetch_factor) | **GET** /v2/Services/{ServiceSid}/Entities/{Identity}/Factors/{Sid} | 
[**fetch_form**](DefaultApi.md#fetch_form) | **GET** /v2/Forms/{FormType} | 
[**fetch_messaging_configuration**](DefaultApi.md#fetch_messaging_configuration) | **GET** /v2/Services/{ServiceSid}/MessagingConfigurations/{Country} | 
[**fetch_rate_limit**](DefaultApi.md#fetch_rate_limit) | **GET** /v2/Services/{ServiceSid}/RateLimits/{Sid} | 
[**fetch_safelist**](DefaultApi.md#fetch_safelist) | **GET** /v2/SafeList/Numbers/{PhoneNumber} | 
[**fetch_service**](DefaultApi.md#fetch_service) | **GET** /v2/Services/{Sid} | 
[**fetch_verification**](DefaultApi.md#fetch_verification) | **GET** /v2/Services/{ServiceSid}/Verifications/{Sid} | 
[**fetch_verification_attempt**](DefaultApi.md#fetch_verification_attempt) | **GET** /v2/Attempts/{Sid} | 
[**fetch_verification_attempts_summary**](DefaultApi.md#fetch_verification_attempts_summary) | **GET** /v2/Attempts/Summary | 
[**fetch_webhook**](DefaultApi.md#fetch_webhook) | **GET** /v2/Services/{ServiceSid}/Webhooks/{Sid} | 
[**list_bucket**](DefaultApi.md#list_bucket) | **GET** /v2/Services/{ServiceSid}/RateLimits/{RateLimitSid}/Buckets | 
[**list_challenge**](DefaultApi.md#list_challenge) | **GET** /v2/Services/{ServiceSid}/Entities/{Identity}/Challenges | 
[**list_entity**](DefaultApi.md#list_entity) | **GET** /v2/Services/{ServiceSid}/Entities | 
[**list_factor**](DefaultApi.md#list_factor) | **GET** /v2/Services/{ServiceSid}/Entities/{Identity}/Factors | 
[**list_messaging_configuration**](DefaultApi.md#list_messaging_configuration) | **GET** /v2/Services/{ServiceSid}/MessagingConfigurations | 
[**list_rate_limit**](DefaultApi.md#list_rate_limit) | **GET** /v2/Services/{ServiceSid}/RateLimits | 
[**list_service**](DefaultApi.md#list_service) | **GET** /v2/Services | 
[**list_verification_attempt**](DefaultApi.md#list_verification_attempt) | **GET** /v2/Attempts | 
[**list_verification_template**](DefaultApi.md#list_verification_template) | **GET** /v2/Templates | 
[**list_webhook**](DefaultApi.md#list_webhook) | **GET** /v2/Services/{ServiceSid}/Webhooks | 
[**update_bucket**](DefaultApi.md#update_bucket) | **POST** /v2/Services/{ServiceSid}/RateLimits/{RateLimitSid}/Buckets/{Sid} | 
[**update_challenge**](DefaultApi.md#update_challenge) | **POST** /v2/Services/{ServiceSid}/Entities/{Identity}/Challenges/{Sid} | 
[**update_factor**](DefaultApi.md#update_factor) | **POST** /v2/Services/{ServiceSid}/Entities/{Identity}/Factors/{Sid} | 
[**update_messaging_configuration**](DefaultApi.md#update_messaging_configuration) | **POST** /v2/Services/{ServiceSid}/MessagingConfigurations/{Country} | 
[**update_rate_limit**](DefaultApi.md#update_rate_limit) | **POST** /v2/Services/{ServiceSid}/RateLimits/{Sid} | 
[**update_service**](DefaultApi.md#update_service) | **POST** /v2/Services/{Sid} | 
[**update_verification**](DefaultApi.md#update_verification) | **POST** /v2/Services/{ServiceSid}/Verifications/{Sid} | 
[**update_webhook**](DefaultApi.md#update_webhook) | **POST** /v2/Services/{ServiceSid}/Webhooks/{Sid} | 


# **create_access_token**
> VerifyV2ServiceAccessToken create_access_token(service_sid, identity, factor_type, factor_friendly_name=factor_friendly_name, ttl=ttl)



Create a new enrollment Access Token for the Entity

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | The unique external identifier for the Entity of the Service. This identifier should be immutable, not PII, and generated by your external system, such as your user's UUID, GUID, or SID.
    factor_type = py_twilio_verify.AccessTokenEnumFactorTypes() # AccessTokenEnumFactorTypes | 
    factor_friendly_name = 'factor_friendly_name_example' # str | The friendly name of the factor that is going to be created with this access token (optional)
    ttl = 56 # int | How long, in seconds, the access token is valid. Can be an integer between 60 and 300. Default is 60. (optional)

    try:
        api_response = api_instance.create_access_token(service_sid, identity, factor_type, factor_friendly_name=factor_friendly_name, ttl=ttl)
        print("The response of DefaultApi->create_access_token:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_access_token: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| The unique external identifier for the Entity of the Service. This identifier should be immutable, not PII, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. | 
 **factor_type** | **AccessTokenEnumFactorTypes**|  | 
 **factor_friendly_name** | **str**| The friendly name of the factor that is going to be created with this access token | [optional] 
 **ttl** | **int**| How long, in seconds, the access token is valid. Can be an integer between 60 and 300. Default is 60. | [optional] 

### Return type

[**VerifyV2ServiceAccessToken**](VerifyV2ServiceAccessToken.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_bucket**
> VerifyV2ServiceRateLimitBucket create_bucket(service_sid, rate_limit_sid, max, interval)



Create a new Bucket for a Rate Limit

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with.
    rate_limit_sid = 'rate_limit_sid_example' # str | The Twilio-provided string that uniquely identifies the Rate Limit resource.
    max = 56 # int | Maximum number of requests permitted in during the interval.
    interval = 56 # int | Number of seconds that the rate limit will be enforced over.

    try:
        api_response = api_instance.create_bucket(service_sid, rate_limit_sid, max, interval)
        print("The response of DefaultApi->create_bucket:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_bucket: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with. | 
 **rate_limit_sid** | **str**| The Twilio-provided string that uniquely identifies the Rate Limit resource. | 
 **max** | **int**| Maximum number of requests permitted in during the interval. | 
 **interval** | **int**| Number of seconds that the rate limit will be enforced over. | 

### Return type

[**VerifyV2ServiceRateLimitBucket**](VerifyV2ServiceRateLimitBucket.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_challenge**
> VerifyV2ServiceEntityChallenge create_challenge(service_sid, identity, factor_sid, expiration_date=expiration_date, details_message=details_message, details_fields=details_fields, hidden_details=hidden_details, auth_payload=auth_payload)



Create a new Challenge for the Factor

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | Customer unique identity for the Entity owner of the Challenge. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.
    factor_sid = 'factor_sid_example' # str | The unique SID identifier of the Factor.
    expiration_date = '2013-10-20T19:20:30+01:00' # datetime | The date-time when this Challenge expires, given in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format. The default value is five (5) minutes after Challenge creation. The max value is sixty (60) minutes after creation. (optional)
    details_message = 'details_message_example' # str | Shown to the user when the push notification arrives. Required when `factor_type` is `push`. Can be up to 256 characters in length (optional)
    details_fields = None # List[object] | A list of objects that describe the Fields included in the Challenge. Each object contains the label and value of the field, the label can be up to 36 characters in length and the value can be up to 128 characters in length. Used when `factor_type` is `push`. There can be up to 20 details fields. (optional)
    hidden_details = None # object | Details provided to give context about the Challenge. Not shown to the end user. It must be a stringified JSON with only strings values eg. `{\\\"ip\\\": \\\"172.168.1.234\\\"}`. Can be up to 1024 characters in length (optional)
    auth_payload = 'auth_payload_example' # str | Optional payload used to verify the Challenge upon creation. Only used with a Factor of type `totp` to carry the TOTP code that needs to be verified. For `TOTP` this value must be between 3 and 8 characters long. (optional)

    try:
        api_response = api_instance.create_challenge(service_sid, identity, factor_sid, expiration_date=expiration_date, details_message=details_message, details_fields=details_fields, hidden_details=hidden_details, auth_payload=auth_payload)
        print("The response of DefaultApi->create_challenge:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_challenge: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| Customer unique identity for the Entity owner of the Challenge. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 
 **factor_sid** | **str**| The unique SID identifier of the Factor. | 
 **expiration_date** | **datetime**| The date-time when this Challenge expires, given in [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) format. The default value is five (5) minutes after Challenge creation. The max value is sixty (60) minutes after creation. | [optional] 
 **details_message** | **str**| Shown to the user when the push notification arrives. Required when &#x60;factor_type&#x60; is &#x60;push&#x60;. Can be up to 256 characters in length | [optional] 
 **details_fields** | [**List[object]**](object.md)| A list of objects that describe the Fields included in the Challenge. Each object contains the label and value of the field, the label can be up to 36 characters in length and the value can be up to 128 characters in length. Used when &#x60;factor_type&#x60; is &#x60;push&#x60;. There can be up to 20 details fields. | [optional] 
 **hidden_details** | [**object**](object.md)| Details provided to give context about the Challenge. Not shown to the end user. It must be a stringified JSON with only strings values eg. &#x60;{\\\&quot;ip\\\&quot;: \\\&quot;172.168.1.234\\\&quot;}&#x60;. Can be up to 1024 characters in length | [optional] 
 **auth_payload** | **str**| Optional payload used to verify the Challenge upon creation. Only used with a Factor of type &#x60;totp&#x60; to carry the TOTP code that needs to be verified. For &#x60;TOTP&#x60; this value must be between 3 and 8 characters long. | [optional] 

### Return type

[**VerifyV2ServiceEntityChallenge**](VerifyV2ServiceEntityChallenge.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_entity**
> VerifyV2ServiceEntity create_entity(service_sid, identity)



Create a new Entity for the Service

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | The unique external identifier for the Entity of the Service. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.

    try:
        api_response = api_instance.create_entity(service_sid, identity)
        print("The response of DefaultApi->create_entity:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_entity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| The unique external identifier for the Entity of the Service. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 

### Return type

[**VerifyV2ServiceEntity**](VerifyV2ServiceEntity.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_messaging_configuration**
> VerifyV2ServiceMessagingConfiguration create_messaging_configuration(service_sid, country, messaging_service_sid)



Create a new MessagingConfiguration for a service.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) that the resource is associated with.
    country = 'country_example' # str | The [ISO-3166-1](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) country code of the country this configuration will be applied to. If this is a global configuration, Country will take the value `all`.
    messaging_service_sid = 'messaging_service_sid_example' # str | The SID of the [Messaging Service](https://www.twilio.com/docs/sms/services/api) to be used to send SMS to the country of this configuration.

    try:
        api_response = api_instance.create_messaging_configuration(service_sid, country, messaging_service_sid)
        print("The response of DefaultApi->create_messaging_configuration:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_messaging_configuration: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) that the resource is associated with. | 
 **country** | **str**| The [ISO-3166-1](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) country code of the country this configuration will be applied to. If this is a global configuration, Country will take the value &#x60;all&#x60;. | 
 **messaging_service_sid** | **str**| The SID of the [Messaging Service](https://www.twilio.com/docs/sms/services/api) to be used to send SMS to the country of this configuration. | 

### Return type

[**VerifyV2ServiceMessagingConfiguration**](VerifyV2ServiceMessagingConfiguration.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_new_factor**
> VerifyV2ServiceEntityNewFactor create_new_factor(service_sid, identity, friendly_name, factor_type, binding_alg=binding_alg, binding_public_key=binding_public_key, config_app_id=config_app_id, config_notification_platform=config_notification_platform, config_notification_token=config_notification_token, config_sdk_version=config_sdk_version, binding_secret=binding_secret, config_time_step=config_time_step, config_skew=config_skew, config_code_length=config_code_length, config_alg=config_alg, metadata=metadata)



Create a new Factor for the Entity

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | Customer unique identity for the Entity owner of the Factor. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.
    friendly_name = 'friendly_name_example' # str | The friendly name of this Factor. This can be any string up to 64 characters, meant for humans to distinguish between Factors. For `factor_type` `push`, this could be a device name. For `factor_type` `totp`, this value is used as the “account name” in constructing the `binding.uri` property. At the same time, we recommend avoiding providing PII.
    factor_type = py_twilio_verify.NewFactorEnumFactorTypes() # NewFactorEnumFactorTypes | 
    binding_alg = 'binding_alg_example' # str | The algorithm used when `factor_type` is `push`. Algorithm supported: `ES256` (optional)
    binding_public_key = 'binding_public_key_example' # str | The Ecdsa public key in PKIX, ASN.1 DER format encoded in Base64.  Required when `factor_type` is `push` (optional)
    config_app_id = 'config_app_id_example' # str | The ID that uniquely identifies your app in the Google or Apple store, such as `com.example.myapp`. It can be up to 100 characters long.  Required when `factor_type` is `push`. (optional)
    config_notification_platform = py_twilio_verify.NewFactorEnumNotificationPlatforms() # NewFactorEnumNotificationPlatforms |  (optional)
    config_notification_token = 'config_notification_token_example' # str | For APN, the device token. For FCM, the registration token. It is used to send the push notifications. Must be between 32 and 255 characters long.  Required when `factor_type` is `push`. (optional)
    config_sdk_version = 'config_sdk_version_example' # str | The Verify Push SDK version used to configure the factor  Required when `factor_type` is `push` (optional)
    binding_secret = 'binding_secret_example' # str | The shared secret for TOTP factors encoded in Base32. This can be provided when creating the Factor, otherwise it will be generated.  Used when `factor_type` is `totp` (optional)
    config_time_step = 56 # int | Defines how often, in seconds, are TOTP codes generated. i.e, a new TOTP code is generated every time_step seconds. Must be between 20 and 60 seconds, inclusive. The default value is defined at the service level in the property `totp.time_step`. Defaults to 30 seconds if not configured.  Used when `factor_type` is `totp` (optional)
    config_skew = 56 # int | The number of time-steps, past and future, that are valid for validation of TOTP codes. Must be between 0 and 2, inclusive. The default value is defined at the service level in the property `totp.skew`. If not configured defaults to 1.  Used when `factor_type` is `totp` (optional)
    config_code_length = 56 # int | Number of digits for generated TOTP codes. Must be between 3 and 8, inclusive. The default value is defined at the service level in the property `totp.code_length`. If not configured defaults to 6.  Used when `factor_type` is `totp` (optional)
    config_alg = py_twilio_verify.NewFactorEnumTotpAlgorithms() # NewFactorEnumTotpAlgorithms |  (optional)
    metadata = None # object | Custom metadata associated with the factor. This is added by the Device/SDK directly to allow for the inclusion of device information. It must be a stringified JSON with only strings values eg. `{\\\"os\\\": \\\"Android\\\"}`. Can be up to 1024 characters in length. (optional)

    try:
        api_response = api_instance.create_new_factor(service_sid, identity, friendly_name, factor_type, binding_alg=binding_alg, binding_public_key=binding_public_key, config_app_id=config_app_id, config_notification_platform=config_notification_platform, config_notification_token=config_notification_token, config_sdk_version=config_sdk_version, binding_secret=binding_secret, config_time_step=config_time_step, config_skew=config_skew, config_code_length=config_code_length, config_alg=config_alg, metadata=metadata)
        print("The response of DefaultApi->create_new_factor:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_new_factor: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| Customer unique identity for the Entity owner of the Factor. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 
 **friendly_name** | **str**| The friendly name of this Factor. This can be any string up to 64 characters, meant for humans to distinguish between Factors. For &#x60;factor_type&#x60; &#x60;push&#x60;, this could be a device name. For &#x60;factor_type&#x60; &#x60;totp&#x60;, this value is used as the “account name” in constructing the &#x60;binding.uri&#x60; property. At the same time, we recommend avoiding providing PII. | 
 **factor_type** | **NewFactorEnumFactorTypes**|  | 
 **binding_alg** | **str**| The algorithm used when &#x60;factor_type&#x60; is &#x60;push&#x60;. Algorithm supported: &#x60;ES256&#x60; | [optional] 
 **binding_public_key** | **str**| The Ecdsa public key in PKIX, ASN.1 DER format encoded in Base64.  Required when &#x60;factor_type&#x60; is &#x60;push&#x60; | [optional] 
 **config_app_id** | **str**| The ID that uniquely identifies your app in the Google or Apple store, such as &#x60;com.example.myapp&#x60;. It can be up to 100 characters long.  Required when &#x60;factor_type&#x60; is &#x60;push&#x60;. | [optional] 
 **config_notification_platform** | **NewFactorEnumNotificationPlatforms**|  | [optional] 
 **config_notification_token** | **str**| For APN, the device token. For FCM, the registration token. It is used to send the push notifications. Must be between 32 and 255 characters long.  Required when &#x60;factor_type&#x60; is &#x60;push&#x60;. | [optional] 
 **config_sdk_version** | **str**| The Verify Push SDK version used to configure the factor  Required when &#x60;factor_type&#x60; is &#x60;push&#x60; | [optional] 
 **binding_secret** | **str**| The shared secret for TOTP factors encoded in Base32. This can be provided when creating the Factor, otherwise it will be generated.  Used when &#x60;factor_type&#x60; is &#x60;totp&#x60; | [optional] 
 **config_time_step** | **int**| Defines how often, in seconds, are TOTP codes generated. i.e, a new TOTP code is generated every time_step seconds. Must be between 20 and 60 seconds, inclusive. The default value is defined at the service level in the property &#x60;totp.time_step&#x60;. Defaults to 30 seconds if not configured.  Used when &#x60;factor_type&#x60; is &#x60;totp&#x60; | [optional] 
 **config_skew** | **int**| The number of time-steps, past and future, that are valid for validation of TOTP codes. Must be between 0 and 2, inclusive. The default value is defined at the service level in the property &#x60;totp.skew&#x60;. If not configured defaults to 1.  Used when &#x60;factor_type&#x60; is &#x60;totp&#x60; | [optional] 
 **config_code_length** | **int**| Number of digits for generated TOTP codes. Must be between 3 and 8, inclusive. The default value is defined at the service level in the property &#x60;totp.code_length&#x60;. If not configured defaults to 6.  Used when &#x60;factor_type&#x60; is &#x60;totp&#x60; | [optional] 
 **config_alg** | **NewFactorEnumTotpAlgorithms**|  | [optional] 
 **metadata** | [**object**](object.md)| Custom metadata associated with the factor. This is added by the Device/SDK directly to allow for the inclusion of device information. It must be a stringified JSON with only strings values eg. &#x60;{\\\&quot;os\\\&quot;: \\\&quot;Android\\\&quot;}&#x60;. Can be up to 1024 characters in length. | [optional] 

### Return type

[**VerifyV2ServiceEntityNewFactor**](VerifyV2ServiceEntityNewFactor.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_notification**
> VerifyV2ServiceEntityChallengeNotification create_notification(service_sid, identity, challenge_sid, ttl=ttl)



Create a new Notification for the corresponding Challenge

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | Customer unique identity for the Entity owner of the Challenge. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.
    challenge_sid = 'challenge_sid_example' # str | The unique SID identifier of the Challenge.
    ttl = 56 # int | How long, in seconds, the notification is valid. Can be an integer between 0 and 300. Default is 300. Delivery is attempted until the TTL elapses, even if the device is offline. 0 means that the notification delivery is attempted immediately, only once, and is not stored for future delivery. (optional)

    try:
        api_response = api_instance.create_notification(service_sid, identity, challenge_sid, ttl=ttl)
        print("The response of DefaultApi->create_notification:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_notification: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| Customer unique identity for the Entity owner of the Challenge. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 
 **challenge_sid** | **str**| The unique SID identifier of the Challenge. | 
 **ttl** | **int**| How long, in seconds, the notification is valid. Can be an integer between 0 and 300. Default is 300. Delivery is attempted until the TTL elapses, even if the device is offline. 0 means that the notification delivery is attempted immediately, only once, and is not stored for future delivery. | [optional] 

### Return type

[**VerifyV2ServiceEntityChallengeNotification**](VerifyV2ServiceEntityChallengeNotification.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_rate_limit**
> VerifyV2ServiceRateLimit create_rate_limit(service_sid, unique_name, description=description)



Create a new Rate Limit for a Service

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with.
    unique_name = 'unique_name_example' # str | Provides a unique and addressable name to be assigned to this Rate Limit, assigned by the developer, to be optionally used in addition to SID. **This value should not contain PII.**
    description = 'description_example' # str | Description of this Rate Limit (optional)

    try:
        api_response = api_instance.create_rate_limit(service_sid, unique_name, description=description)
        print("The response of DefaultApi->create_rate_limit:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_rate_limit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with. | 
 **unique_name** | **str**| Provides a unique and addressable name to be assigned to this Rate Limit, assigned by the developer, to be optionally used in addition to SID. **This value should not contain PII.** | 
 **description** | **str**| Description of this Rate Limit | [optional] 

### Return type

[**VerifyV2ServiceRateLimit**](VerifyV2ServiceRateLimit.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_safelist**
> VerifyV2Safelist create_safelist(phone_number)



Add a new phone number to SafeList.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    phone_number = 'phone_number_example' # str | The phone number to be added in SafeList. Phone numbers must be in [E.164 format](https://www.twilio.com/docs/glossary/what-e164).

    try:
        api_response = api_instance.create_safelist(phone_number)
        print("The response of DefaultApi->create_safelist:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_safelist: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone_number** | **str**| The phone number to be added in SafeList. Phone numbers must be in [E.164 format](https://www.twilio.com/docs/glossary/what-e164). | 

### Return type

[**VerifyV2Safelist**](VerifyV2Safelist.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_service**
> VerifyV2Service create_service(friendly_name, code_length=code_length, lookup_enabled=lookup_enabled, skip_sms_to_landlines=skip_sms_to_landlines, dtmf_input_required=dtmf_input_required, tts_name=tts_name, psd2_enabled=psd2_enabled, do_not_share_warning_enabled=do_not_share_warning_enabled, custom_code_enabled=custom_code_enabled, push_include_date=push_include_date, push_apn_credential_sid=push_apn_credential_sid, push_fcm_credential_sid=push_fcm_credential_sid, totp_issuer=totp_issuer, totp_time_step=totp_time_step, totp_code_length=totp_code_length, totp_skew=totp_skew, default_template_sid=default_template_sid)



Create a new Verification Service.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    friendly_name = 'friendly_name_example' # str | A descriptive string that you create to describe the verification service. It can be up to 30 characters long. **This value should not contain PII.**
    code_length = 56 # int | The length of the verification code to generate. Must be an integer value between 4 and 10, inclusive. (optional)
    lookup_enabled = True # bool | Whether to perform a lookup with each verification started and return info about the phone number. (optional)
    skip_sms_to_landlines = True # bool | Whether to skip sending SMS verifications to landlines. Requires `lookup_enabled`. (optional)
    dtmf_input_required = True # bool | Whether to ask the user to press a number before delivering the verify code in a phone call. (optional)
    tts_name = 'tts_name_example' # str | The name of an alternative text-to-speech service to use in phone calls. Applies only to TTS languages. (optional)
    psd2_enabled = True # bool | Whether to pass PSD2 transaction parameters when starting a verification. (optional)
    do_not_share_warning_enabled = True # bool | Whether to add a security warning at the end of an SMS verification body. Disabled by default and applies only to SMS. Example SMS body: `Your AppName verification code is: 1234. Don’t share this code with anyone; our employees will never ask for the code` (optional)
    custom_code_enabled = True # bool | Whether to allow sending verifications with a custom code instead of a randomly generated one. Not available for all customers. (optional)
    push_include_date = True # bool | Optional configuration for the Push factors. If true, include the date in the Challenge's response. Otherwise, the date is omitted from the response. See [Challenge](https://www.twilio.com/docs/verify/api/challenge) resource’s details parameter for more info. Default: false. **Deprecated** do not use this parameter. This timestamp value is the same one as the one found in `date_created`, please use that one instead. (optional)
    push_apn_credential_sid = 'push_apn_credential_sid_example' # str | Optional configuration for the Push factors. Set the APN Credential for this service. This will allow to send push notifications to iOS devices. See [Credential Resource](https://www.twilio.com/docs/notify/api/credential-resource) (optional)
    push_fcm_credential_sid = 'push_fcm_credential_sid_example' # str | Optional configuration for the Push factors. Set the FCM Credential for this service. This will allow to send push notifications to Android devices. See [Credential Resource](https://www.twilio.com/docs/notify/api/credential-resource) (optional)
    totp_issuer = 'totp_issuer_example' # str | Optional configuration for the TOTP factors. Set TOTP Issuer for this service. This will allow to configure the issuer of the TOTP URI. Defaults to the service friendly name if not provided. (optional)
    totp_time_step = 56 # int | Optional configuration for the TOTP factors. Defines how often, in seconds, are TOTP codes generated. i.e, a new TOTP code is generated every time_step seconds. Must be between 20 and 60 seconds, inclusive. Defaults to 30 seconds (optional)
    totp_code_length = 56 # int | Optional configuration for the TOTP factors. Number of digits for generated TOTP codes. Must be between 3 and 8, inclusive. Defaults to 6 (optional)
    totp_skew = 56 # int | Optional configuration for the TOTP factors. The number of time-steps, past and future, that are valid for validation of TOTP codes. Must be between 0 and 2, inclusive. Defaults to 1 (optional)
    default_template_sid = 'default_template_sid_example' # str | The default message [template](https://www.twilio.com/docs/verify/api/templates). Will be used for all SMS verifications unless explicitly overriden. SMS channel only. (optional)

    try:
        api_response = api_instance.create_service(friendly_name, code_length=code_length, lookup_enabled=lookup_enabled, skip_sms_to_landlines=skip_sms_to_landlines, dtmf_input_required=dtmf_input_required, tts_name=tts_name, psd2_enabled=psd2_enabled, do_not_share_warning_enabled=do_not_share_warning_enabled, custom_code_enabled=custom_code_enabled, push_include_date=push_include_date, push_apn_credential_sid=push_apn_credential_sid, push_fcm_credential_sid=push_fcm_credential_sid, totp_issuer=totp_issuer, totp_time_step=totp_time_step, totp_code_length=totp_code_length, totp_skew=totp_skew, default_template_sid=default_template_sid)
        print("The response of DefaultApi->create_service:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_service: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **friendly_name** | **str**| A descriptive string that you create to describe the verification service. It can be up to 30 characters long. **This value should not contain PII.** | 
 **code_length** | **int**| The length of the verification code to generate. Must be an integer value between 4 and 10, inclusive. | [optional] 
 **lookup_enabled** | **bool**| Whether to perform a lookup with each verification started and return info about the phone number. | [optional] 
 **skip_sms_to_landlines** | **bool**| Whether to skip sending SMS verifications to landlines. Requires &#x60;lookup_enabled&#x60;. | [optional] 
 **dtmf_input_required** | **bool**| Whether to ask the user to press a number before delivering the verify code in a phone call. | [optional] 
 **tts_name** | **str**| The name of an alternative text-to-speech service to use in phone calls. Applies only to TTS languages. | [optional] 
 **psd2_enabled** | **bool**| Whether to pass PSD2 transaction parameters when starting a verification. | [optional] 
 **do_not_share_warning_enabled** | **bool**| Whether to add a security warning at the end of an SMS verification body. Disabled by default and applies only to SMS. Example SMS body: &#x60;Your AppName verification code is: 1234. Don’t share this code with anyone; our employees will never ask for the code&#x60; | [optional] 
 **custom_code_enabled** | **bool**| Whether to allow sending verifications with a custom code instead of a randomly generated one. Not available for all customers. | [optional] 
 **push_include_date** | **bool**| Optional configuration for the Push factors. If true, include the date in the Challenge&#39;s response. Otherwise, the date is omitted from the response. See [Challenge](https://www.twilio.com/docs/verify/api/challenge) resource’s details parameter for more info. Default: false. **Deprecated** do not use this parameter. This timestamp value is the same one as the one found in &#x60;date_created&#x60;, please use that one instead. | [optional] 
 **push_apn_credential_sid** | **str**| Optional configuration for the Push factors. Set the APN Credential for this service. This will allow to send push notifications to iOS devices. See [Credential Resource](https://www.twilio.com/docs/notify/api/credential-resource) | [optional] 
 **push_fcm_credential_sid** | **str**| Optional configuration for the Push factors. Set the FCM Credential for this service. This will allow to send push notifications to Android devices. See [Credential Resource](https://www.twilio.com/docs/notify/api/credential-resource) | [optional] 
 **totp_issuer** | **str**| Optional configuration for the TOTP factors. Set TOTP Issuer for this service. This will allow to configure the issuer of the TOTP URI. Defaults to the service friendly name if not provided. | [optional] 
 **totp_time_step** | **int**| Optional configuration for the TOTP factors. Defines how often, in seconds, are TOTP codes generated. i.e, a new TOTP code is generated every time_step seconds. Must be between 20 and 60 seconds, inclusive. Defaults to 30 seconds | [optional] 
 **totp_code_length** | **int**| Optional configuration for the TOTP factors. Number of digits for generated TOTP codes. Must be between 3 and 8, inclusive. Defaults to 6 | [optional] 
 **totp_skew** | **int**| Optional configuration for the TOTP factors. The number of time-steps, past and future, that are valid for validation of TOTP codes. Must be between 0 and 2, inclusive. Defaults to 1 | [optional] 
 **default_template_sid** | **str**| The default message [template](https://www.twilio.com/docs/verify/api/templates). Will be used for all SMS verifications unless explicitly overriden. SMS channel only. | [optional] 

### Return type

[**VerifyV2Service**](VerifyV2Service.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_verification**
> VerifyV2ServiceVerification create_verification(service_sid, to, channel, custom_friendly_name=custom_friendly_name, custom_message=custom_message, send_digits=send_digits, locale=locale, custom_code=custom_code, amount=amount, payee=payee, rate_limits=rate_limits, channel_configuration=channel_configuration, app_hash=app_hash, template_sid=template_sid, template_custom_substitutions=template_custom_substitutions)



Create a new Verification using a Service

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the verification [Service](https://www.twilio.com/docs/verify/api/service) to create the resource under.
    to = 'to_example' # str | The phone number or [email](https://www.twilio.com/docs/verify/email) to verify. Phone numbers must be in [E.164 format](https://www.twilio.com/docs/glossary/what-e164).
    channel = 'channel_example' # str | The verification method to use. One of: [`email`](https://www.twilio.com/docs/verify/email), `sms`, `whatsapp`, `call`, or `sna`.
    custom_friendly_name = 'custom_friendly_name_example' # str | A custom user defined friendly name that overwrites the existing one in the verification message (optional)
    custom_message = 'custom_message_example' # str | The text of a custom message to use for the verification. (optional)
    send_digits = 'send_digits_example' # str | The digits to send after a phone call is answered, for example, to dial an extension. For more information, see the Programmable Voice documentation of [sendDigits](https://www.twilio.com/docs/voice/twiml/number#attributes-sendDigits). (optional)
    locale = 'locale_example' # str | Locale will automatically resolve based on phone number country code for SMS, WhatsApp and call channel verifications. This parameter will override the automatic locale. [See supported languages and more information here](https://www.twilio.com/docs/verify/supported-languages). (optional)
    custom_code = 'custom_code_example' # str | A pre-generated code to use for verification. The code can be between 4 and 10 characters, inclusive. (optional)
    amount = 'amount_example' # str | The amount of the associated PSD2 compliant transaction. Requires the PSD2 Service flag enabled. (optional)
    payee = 'payee_example' # str | The payee of the associated PSD2 compliant transaction. Requires the PSD2 Service flag enabled. (optional)
    rate_limits = None # object | The custom key-value pairs of Programmable Rate Limits. Keys correspond to `unique_name` fields defined when [creating your Rate Limit](https://www.twilio.com/docs/verify/api/service-rate-limits). Associated value pairs represent values in the request that you are rate limiting on. You may include multiple Rate Limit values in each request. (optional)
    channel_configuration = None # object | [`email`](https://www.twilio.com/docs/verify/email) channel configuration in json format. The fields 'from' and 'from_name' are optional but if included the 'from' field must have a valid email address. (optional)
    app_hash = 'app_hash_example' # str | Your [App Hash](https://developers.google.com/identity/sms-retriever/verify#computing_your_apps_hash_string) to be appended at the end of your verification SMS body. Applies only to SMS. Example SMS body: `<#> Your AppName verification code is: 1234 He42w354ol9`. (optional)
    template_sid = 'template_sid_example' # str | The message [template](https://www.twilio.com/docs/verify/api/templates). If provided, will override the default template for the Service. SMS and Voice channels only. (optional)
    template_custom_substitutions = 'template_custom_substitutions_example' # str | A stringified JSON object in which the keys are the template's special variables and the values are the variables substitutions. (optional)

    try:
        api_response = api_instance.create_verification(service_sid, to, channel, custom_friendly_name=custom_friendly_name, custom_message=custom_message, send_digits=send_digits, locale=locale, custom_code=custom_code, amount=amount, payee=payee, rate_limits=rate_limits, channel_configuration=channel_configuration, app_hash=app_hash, template_sid=template_sid, template_custom_substitutions=template_custom_substitutions)
        print("The response of DefaultApi->create_verification:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_verification: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the verification [Service](https://www.twilio.com/docs/verify/api/service) to create the resource under. | 
 **to** | **str**| The phone number or [email](https://www.twilio.com/docs/verify/email) to verify. Phone numbers must be in [E.164 format](https://www.twilio.com/docs/glossary/what-e164). | 
 **channel** | **str**| The verification method to use. One of: [&#x60;email&#x60;](https://www.twilio.com/docs/verify/email), &#x60;sms&#x60;, &#x60;whatsapp&#x60;, &#x60;call&#x60;, or &#x60;sna&#x60;. | 
 **custom_friendly_name** | **str**| A custom user defined friendly name that overwrites the existing one in the verification message | [optional] 
 **custom_message** | **str**| The text of a custom message to use for the verification. | [optional] 
 **send_digits** | **str**| The digits to send after a phone call is answered, for example, to dial an extension. For more information, see the Programmable Voice documentation of [sendDigits](https://www.twilio.com/docs/voice/twiml/number#attributes-sendDigits). | [optional] 
 **locale** | **str**| Locale will automatically resolve based on phone number country code for SMS, WhatsApp and call channel verifications. This parameter will override the automatic locale. [See supported languages and more information here](https://www.twilio.com/docs/verify/supported-languages). | [optional] 
 **custom_code** | **str**| A pre-generated code to use for verification. The code can be between 4 and 10 characters, inclusive. | [optional] 
 **amount** | **str**| The amount of the associated PSD2 compliant transaction. Requires the PSD2 Service flag enabled. | [optional] 
 **payee** | **str**| The payee of the associated PSD2 compliant transaction. Requires the PSD2 Service flag enabled. | [optional] 
 **rate_limits** | [**object**](object.md)| The custom key-value pairs of Programmable Rate Limits. Keys correspond to &#x60;unique_name&#x60; fields defined when [creating your Rate Limit](https://www.twilio.com/docs/verify/api/service-rate-limits). Associated value pairs represent values in the request that you are rate limiting on. You may include multiple Rate Limit values in each request. | [optional] 
 **channel_configuration** | [**object**](object.md)| [&#x60;email&#x60;](https://www.twilio.com/docs/verify/email) channel configuration in json format. The fields &#39;from&#39; and &#39;from_name&#39; are optional but if included the &#39;from&#39; field must have a valid email address. | [optional] 
 **app_hash** | **str**| Your [App Hash](https://developers.google.com/identity/sms-retriever/verify#computing_your_apps_hash_string) to be appended at the end of your verification SMS body. Applies only to SMS. Example SMS body: &#x60;&lt;#&gt; Your AppName verification code is: 1234 He42w354ol9&#x60;. | [optional] 
 **template_sid** | **str**| The message [template](https://www.twilio.com/docs/verify/api/templates). If provided, will override the default template for the Service. SMS and Voice channels only. | [optional] 
 **template_custom_substitutions** | **str**| A stringified JSON object in which the keys are the template&#39;s special variables and the values are the variables substitutions. | [optional] 

### Return type

[**VerifyV2ServiceVerification**](VerifyV2ServiceVerification.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_verification_check**
> VerifyV2ServiceVerificationCheck create_verification_check(service_sid, code=code, to=to, verification_sid=verification_sid, amount=amount, payee=payee)



challenge a specific Verification Check.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the verification [Service](https://www.twilio.com/docs/verify/api/service) to create the resource under.
    code = 'code_example' # str | The 4-10 character string being verified. (optional)
    to = 'to_example' # str | The phone number or [email](https://www.twilio.com/docs/verify/email) to verify. Either this parameter or the `verification_sid` must be specified. Phone numbers must be in [E.164 format](https://www.twilio.com/docs/glossary/what-e164). (optional)
    verification_sid = 'verification_sid_example' # str | A SID that uniquely identifies the Verification Check. Either this parameter or the `to` phone number/[email](https://www.twilio.com/docs/verify/email) must be specified. (optional)
    amount = 'amount_example' # str | The amount of the associated PSD2 compliant transaction. Requires the PSD2 Service flag enabled. (optional)
    payee = 'payee_example' # str | The payee of the associated PSD2 compliant transaction. Requires the PSD2 Service flag enabled. (optional)

    try:
        api_response = api_instance.create_verification_check(service_sid, code=code, to=to, verification_sid=verification_sid, amount=amount, payee=payee)
        print("The response of DefaultApi->create_verification_check:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_verification_check: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the verification [Service](https://www.twilio.com/docs/verify/api/service) to create the resource under. | 
 **code** | **str**| The 4-10 character string being verified. | [optional] 
 **to** | **str**| The phone number or [email](https://www.twilio.com/docs/verify/email) to verify. Either this parameter or the &#x60;verification_sid&#x60; must be specified. Phone numbers must be in [E.164 format](https://www.twilio.com/docs/glossary/what-e164). | [optional] 
 **verification_sid** | **str**| A SID that uniquely identifies the Verification Check. Either this parameter or the &#x60;to&#x60; phone number/[email](https://www.twilio.com/docs/verify/email) must be specified. | [optional] 
 **amount** | **str**| The amount of the associated PSD2 compliant transaction. Requires the PSD2 Service flag enabled. | [optional] 
 **payee** | **str**| The payee of the associated PSD2 compliant transaction. Requires the PSD2 Service flag enabled. | [optional] 

### Return type

[**VerifyV2ServiceVerificationCheck**](VerifyV2ServiceVerificationCheck.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **create_webhook**
> VerifyV2ServiceWebhook create_webhook(service_sid, friendly_name, event_types, webhook_url, status=status, version=version)



Create a new Webhook for the Service

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    friendly_name = 'friendly_name_example' # str | The string that you assigned to describe the webhook. **This value should not contain PII.**
    event_types = ['event_types_example'] # List[str] | The array of events that this Webhook is subscribed to. Possible event types: `*, factor.deleted, factor.created, factor.verified, challenge.approved, challenge.denied`
    webhook_url = 'webhook_url_example' # str | The URL associated with this Webhook.
    status = py_twilio_verify.WebhookEnumStatus() # WebhookEnumStatus |  (optional)
    version = py_twilio_verify.WebhookEnumVersion() # WebhookEnumVersion |  (optional)

    try:
        api_response = api_instance.create_webhook(service_sid, friendly_name, event_types, webhook_url, status=status, version=version)
        print("The response of DefaultApi->create_webhook:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->create_webhook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **friendly_name** | **str**| The string that you assigned to describe the webhook. **This value should not contain PII.** | 
 **event_types** | [**List[str]**](str.md)| The array of events that this Webhook is subscribed to. Possible event types: &#x60;*, factor.deleted, factor.created, factor.verified, challenge.approved, challenge.denied&#x60; | 
 **webhook_url** | **str**| The URL associated with this Webhook. | 
 **status** | **WebhookEnumStatus**|  | [optional] 
 **version** | **WebhookEnumVersion**|  | [optional] 

### Return type

[**VerifyV2ServiceWebhook**](VerifyV2ServiceWebhook.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**201** | Created |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_bucket**
> delete_bucket(service_sid, rate_limit_sid, sid)



Delete a specific Bucket.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with.
    rate_limit_sid = 'rate_limit_sid_example' # str | The Twilio-provided string that uniquely identifies the Rate Limit resource.
    sid = 'sid_example' # str | A 34 character string that uniquely identifies this Bucket.

    try:
        api_instance.delete_bucket(service_sid, rate_limit_sid, sid)
    except Exception as e:
        print("Exception when calling DefaultApi->delete_bucket: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with. | 
 **rate_limit_sid** | **str**| The Twilio-provided string that uniquely identifies the Rate Limit resource. | 
 **sid** | **str**| A 34 character string that uniquely identifies this Bucket. | 

### Return type

void (empty response body)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | The resource was deleted successfully. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_entity**
> delete_entity(service_sid, identity)



Delete a specific Entity.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | The unique external identifier for the Entity of the Service. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.

    try:
        api_instance.delete_entity(service_sid, identity)
    except Exception as e:
        print("Exception when calling DefaultApi->delete_entity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| The unique external identifier for the Entity of the Service. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 

### Return type

void (empty response body)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | The resource was deleted successfully. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_factor**
> delete_factor(service_sid, identity, sid)



Delete a specific Factor.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | Customer unique identity for the Entity owner of the Factor. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.
    sid = 'sid_example' # str | A 34 character string that uniquely identifies this Factor.

    try:
        api_instance.delete_factor(service_sid, identity, sid)
    except Exception as e:
        print("Exception when calling DefaultApi->delete_factor: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| Customer unique identity for the Entity owner of the Factor. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 
 **sid** | **str**| A 34 character string that uniquely identifies this Factor. | 

### Return type

void (empty response body)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | The resource was deleted successfully. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_messaging_configuration**
> delete_messaging_configuration(service_sid, country)



Delete a specific MessagingConfiguration.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) that the resource is associated with.
    country = 'country_example' # str | The [ISO-3166-1](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) country code of the country this configuration will be applied to. If this is a global configuration, Country will take the value `all`.

    try:
        api_instance.delete_messaging_configuration(service_sid, country)
    except Exception as e:
        print("Exception when calling DefaultApi->delete_messaging_configuration: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) that the resource is associated with. | 
 **country** | **str**| The [ISO-3166-1](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) country code of the country this configuration will be applied to. If this is a global configuration, Country will take the value &#x60;all&#x60;. | 

### Return type

void (empty response body)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | The resource was deleted successfully. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_rate_limit**
> delete_rate_limit(service_sid, sid)



Delete a specific Rate Limit.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with.
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the Rate Limit resource to fetch.

    try:
        api_instance.delete_rate_limit(service_sid, sid)
    except Exception as e:
        print("Exception when calling DefaultApi->delete_rate_limit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with. | 
 **sid** | **str**| The Twilio-provided string that uniquely identifies the Rate Limit resource to fetch. | 

### Return type

void (empty response body)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | The resource was deleted successfully. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_safelist**
> delete_safelist(phone_number)



Remove a phone number from SafeList.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    phone_number = 'phone_number_example' # str | The phone number to be removed from SafeList. Phone numbers must be in [E.164 format](https://www.twilio.com/docs/glossary/what-e164).

    try:
        api_instance.delete_safelist(phone_number)
    except Exception as e:
        print("Exception when calling DefaultApi->delete_safelist: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone_number** | **str**| The phone number to be removed from SafeList. Phone numbers must be in [E.164 format](https://www.twilio.com/docs/glossary/what-e164). | 

### Return type

void (empty response body)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | The resource was deleted successfully. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_service**
> delete_service(sid)



Delete a specific Verification Service Instance.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the Verification Service resource to delete.

    try:
        api_instance.delete_service(sid)
    except Exception as e:
        print("Exception when calling DefaultApi->delete_service: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sid** | **str**| The Twilio-provided string that uniquely identifies the Verification Service resource to delete. | 

### Return type

void (empty response body)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | The resource was deleted successfully. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **delete_webhook**
> delete_webhook(service_sid, sid)



Delete a specific Webhook.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the Webhook resource to delete.

    try:
        api_instance.delete_webhook(service_sid, sid)
    except Exception as e:
        print("Exception when calling DefaultApi->delete_webhook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **sid** | **str**| The Twilio-provided string that uniquely identifies the Webhook resource to delete. | 

### Return type

void (empty response body)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**204** | The resource was deleted successfully. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_access_token**
> VerifyV2ServiceAccessToken fetch_access_token(service_sid, sid)



Fetch an Access Token for the Entity

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    sid = 'sid_example' # str | A 34 character string that uniquely identifies this Access Token.

    try:
        api_response = api_instance.fetch_access_token(service_sid, sid)
        print("The response of DefaultApi->fetch_access_token:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_access_token: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **sid** | **str**| A 34 character string that uniquely identifies this Access Token. | 

### Return type

[**VerifyV2ServiceAccessToken**](VerifyV2ServiceAccessToken.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_bucket**
> VerifyV2ServiceRateLimitBucket fetch_bucket(service_sid, rate_limit_sid, sid)



Fetch a specific Bucket.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with.
    rate_limit_sid = 'rate_limit_sid_example' # str | The Twilio-provided string that uniquely identifies the Rate Limit resource.
    sid = 'sid_example' # str | A 34 character string that uniquely identifies this Bucket.

    try:
        api_response = api_instance.fetch_bucket(service_sid, rate_limit_sid, sid)
        print("The response of DefaultApi->fetch_bucket:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_bucket: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with. | 
 **rate_limit_sid** | **str**| The Twilio-provided string that uniquely identifies the Rate Limit resource. | 
 **sid** | **str**| A 34 character string that uniquely identifies this Bucket. | 

### Return type

[**VerifyV2ServiceRateLimitBucket**](VerifyV2ServiceRateLimitBucket.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_challenge**
> VerifyV2ServiceEntityChallenge fetch_challenge(service_sid, identity, sid)



Fetch a specific Challenge.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | Customer unique identity for the Entity owner of the Challenges. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.
    sid = 'sid_example' # str | A 34 character string that uniquely identifies this Challenge.

    try:
        api_response = api_instance.fetch_challenge(service_sid, identity, sid)
        print("The response of DefaultApi->fetch_challenge:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_challenge: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| Customer unique identity for the Entity owner of the Challenges. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 
 **sid** | **str**| A 34 character string that uniquely identifies this Challenge. | 

### Return type

[**VerifyV2ServiceEntityChallenge**](VerifyV2ServiceEntityChallenge.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_entity**
> VerifyV2ServiceEntity fetch_entity(service_sid, identity)



Fetch a specific Entity.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | The unique external identifier for the Entity of the Service. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.

    try:
        api_response = api_instance.fetch_entity(service_sid, identity)
        print("The response of DefaultApi->fetch_entity:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_entity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| The unique external identifier for the Entity of the Service. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 

### Return type

[**VerifyV2ServiceEntity**](VerifyV2ServiceEntity.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_factor**
> VerifyV2ServiceEntityFactor fetch_factor(service_sid, identity, sid)



Fetch a specific Factor.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | Customer unique identity for the Entity owner of the Factor. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.
    sid = 'sid_example' # str | A 34 character string that uniquely identifies this Factor.

    try:
        api_response = api_instance.fetch_factor(service_sid, identity, sid)
        print("The response of DefaultApi->fetch_factor:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_factor: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| Customer unique identity for the Entity owner of the Factor. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 
 **sid** | **str**| A 34 character string that uniquely identifies this Factor. | 

### Return type

[**VerifyV2ServiceEntityFactor**](VerifyV2ServiceEntityFactor.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_form**
> VerifyV2Form fetch_form(form_type)



Fetch the forms for a specific Form Type.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    form_type = py_twilio_verify.FormEnumFormTypes() # FormEnumFormTypes | The Type of this Form. Currently only `form-push` is supported.

    try:
        api_response = api_instance.fetch_form(form_type)
        print("The response of DefaultApi->fetch_form:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_form: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **form_type** | **FormEnumFormTypes**| The Type of this Form. Currently only &#x60;form-push&#x60; is supported. | 

### Return type

[**VerifyV2Form**](VerifyV2Form.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_messaging_configuration**
> VerifyV2ServiceMessagingConfiguration fetch_messaging_configuration(service_sid, country)



Fetch a specific MessagingConfiguration.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) that the resource is associated with.
    country = 'country_example' # str | The [ISO-3166-1](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) country code of the country this configuration will be applied to. If this is a global configuration, Country will take the value `all`.

    try:
        api_response = api_instance.fetch_messaging_configuration(service_sid, country)
        print("The response of DefaultApi->fetch_messaging_configuration:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_messaging_configuration: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) that the resource is associated with. | 
 **country** | **str**| The [ISO-3166-1](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) country code of the country this configuration will be applied to. If this is a global configuration, Country will take the value &#x60;all&#x60;. | 

### Return type

[**VerifyV2ServiceMessagingConfiguration**](VerifyV2ServiceMessagingConfiguration.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_rate_limit**
> VerifyV2ServiceRateLimit fetch_rate_limit(service_sid, sid)



Fetch a specific Rate Limit.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with.
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the Rate Limit resource to fetch.

    try:
        api_response = api_instance.fetch_rate_limit(service_sid, sid)
        print("The response of DefaultApi->fetch_rate_limit:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_rate_limit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with. | 
 **sid** | **str**| The Twilio-provided string that uniquely identifies the Rate Limit resource to fetch. | 

### Return type

[**VerifyV2ServiceRateLimit**](VerifyV2ServiceRateLimit.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_safelist**
> VerifyV2Safelist fetch_safelist(phone_number)



Check if a phone number exists in SafeList.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    phone_number = 'phone_number_example' # str | The phone number to be fetched from SafeList. Phone numbers must be in [E.164 format](https://www.twilio.com/docs/glossary/what-e164).

    try:
        api_response = api_instance.fetch_safelist(phone_number)
        print("The response of DefaultApi->fetch_safelist:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_safelist: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phone_number** | **str**| The phone number to be fetched from SafeList. Phone numbers must be in [E.164 format](https://www.twilio.com/docs/glossary/what-e164). | 

### Return type

[**VerifyV2Safelist**](VerifyV2Safelist.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_service**
> VerifyV2Service fetch_service(sid)



Fetch specific Verification Service Instance.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the Verification Service resource to fetch.

    try:
        api_response = api_instance.fetch_service(sid)
        print("The response of DefaultApi->fetch_service:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_service: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sid** | **str**| The Twilio-provided string that uniquely identifies the Verification Service resource to fetch. | 

### Return type

[**VerifyV2Service**](VerifyV2Service.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_verification**
> VerifyV2ServiceVerification fetch_verification(service_sid, sid)



Fetch a specific Verification

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the verification [Service](https://www.twilio.com/docs/verify/api/service) to fetch the resource from.
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the Verification resource to fetch.

    try:
        api_response = api_instance.fetch_verification(service_sid, sid)
        print("The response of DefaultApi->fetch_verification:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_verification: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the verification [Service](https://www.twilio.com/docs/verify/api/service) to fetch the resource from. | 
 **sid** | **str**| The Twilio-provided string that uniquely identifies the Verification resource to fetch. | 

### Return type

[**VerifyV2ServiceVerification**](VerifyV2ServiceVerification.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_verification_attempt**
> VerifyV2VerificationAttempt fetch_verification_attempt(sid)



Fetch a specific verification attempt.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    sid = 'sid_example' # str | The unique SID identifier of a Verification Attempt

    try:
        api_response = api_instance.fetch_verification_attempt(sid)
        print("The response of DefaultApi->fetch_verification_attempt:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_verification_attempt: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sid** | **str**| The unique SID identifier of a Verification Attempt | 

### Return type

[**VerifyV2VerificationAttempt**](VerifyV2VerificationAttempt.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_verification_attempts_summary**
> VerifyV2VerificationAttemptsSummary fetch_verification_attempts_summary(verify_service_sid=verify_service_sid, date_created_after=date_created_after, date_created_before=date_created_before, country=country, channel=channel, destination_prefix=destination_prefix)



Get a summary of how many attempts were made and how many were converted.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    verify_service_sid = 'verify_service_sid_example' # str | Filter used to consider only Verification Attempts of the given verify service on the summary aggregation. (optional)
    date_created_after = '2013-10-20T19:20:30+01:00' # datetime | Datetime filter used to consider only Verification Attempts created after this datetime on the summary aggregation. Given as GMT in RFC 2822 format. (optional)
    date_created_before = '2013-10-20T19:20:30+01:00' # datetime | Datetime filter used to consider only Verification Attempts created before this datetime on the summary aggregation. Given as GMT in RFC 2822 format. (optional)
    country = 'country_example' # str | Filter used to consider only Verification Attempts sent to the specified destination country on the summary aggregation. (optional)
    channel = py_twilio_verify.VerificationAttemptsSummaryEnumChannels() # VerificationAttemptsSummaryEnumChannels | Filter Verification Attempts considered on the summary aggregation by communication channel. Valid values are `SMS` and `CALL` (optional)
    destination_prefix = 'destination_prefix_example' # str | Filter the Verification Attempts considered on the summary aggregation by Destination prefix. It is the prefix of a phone number in E.164 format. (optional)

    try:
        api_response = api_instance.fetch_verification_attempts_summary(verify_service_sid=verify_service_sid, date_created_after=date_created_after, date_created_before=date_created_before, country=country, channel=channel, destination_prefix=destination_prefix)
        print("The response of DefaultApi->fetch_verification_attempts_summary:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_verification_attempts_summary: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **verify_service_sid** | **str**| Filter used to consider only Verification Attempts of the given verify service on the summary aggregation. | [optional] 
 **date_created_after** | **datetime**| Datetime filter used to consider only Verification Attempts created after this datetime on the summary aggregation. Given as GMT in RFC 2822 format. | [optional] 
 **date_created_before** | **datetime**| Datetime filter used to consider only Verification Attempts created before this datetime on the summary aggregation. Given as GMT in RFC 2822 format. | [optional] 
 **country** | **str**| Filter used to consider only Verification Attempts sent to the specified destination country on the summary aggregation. | [optional] 
 **channel** | **VerificationAttemptsSummaryEnumChannels**| Filter Verification Attempts considered on the summary aggregation by communication channel. Valid values are &#x60;SMS&#x60; and &#x60;CALL&#x60; | [optional] 
 **destination_prefix** | **str**| Filter the Verification Attempts considered on the summary aggregation by Destination prefix. It is the prefix of a phone number in E.164 format. | [optional] 

### Return type

[**VerifyV2VerificationAttemptsSummary**](VerifyV2VerificationAttemptsSummary.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fetch_webhook**
> VerifyV2ServiceWebhook fetch_webhook(service_sid, sid)



Fetch a specific Webhook.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the Webhook resource to fetch.

    try:
        api_response = api_instance.fetch_webhook(service_sid, sid)
        print("The response of DefaultApi->fetch_webhook:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fetch_webhook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **sid** | **str**| The Twilio-provided string that uniquely identifies the Webhook resource to fetch. | 

### Return type

[**VerifyV2ServiceWebhook**](VerifyV2ServiceWebhook.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_bucket**
> ListBucketResponse list_bucket(service_sid, rate_limit_sid, page_size=page_size)



Retrieve a list of all Buckets for a Rate Limit.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with.
    rate_limit_sid = 'rate_limit_sid_example' # str | The Twilio-provided string that uniquely identifies the Rate Limit resource.
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_bucket(service_sid, rate_limit_sid, page_size=page_size)
        print("The response of DefaultApi->list_bucket:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_bucket: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with. | 
 **rate_limit_sid** | **str**| The Twilio-provided string that uniquely identifies the Rate Limit resource. | 
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListBucketResponse**](ListBucketResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_challenge**
> ListChallengeResponse list_challenge(service_sid, identity, factor_sid=factor_sid, status=status, order=order, page_size=page_size)



Retrieve a list of all Challenges for a Factor.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | Customer unique identity for the Entity owner of the Challenge. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.
    factor_sid = 'factor_sid_example' # str | The unique SID identifier of the Factor. (optional)
    status = py_twilio_verify.ChallengeEnumChallengeStatuses() # ChallengeEnumChallengeStatuses | The Status of the Challenges to fetch. One of `pending`, `expired`, `approved` or `denied`. (optional)
    order = py_twilio_verify.ChallengeEnumListOrders() # ChallengeEnumListOrders | The desired sort order of the Challenges list. One of `asc` or `desc` for ascending and descending respectively. Defaults to `asc`. (optional)
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_challenge(service_sid, identity, factor_sid=factor_sid, status=status, order=order, page_size=page_size)
        print("The response of DefaultApi->list_challenge:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_challenge: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| Customer unique identity for the Entity owner of the Challenge. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 
 **factor_sid** | **str**| The unique SID identifier of the Factor. | [optional] 
 **status** | **ChallengeEnumChallengeStatuses**| The Status of the Challenges to fetch. One of &#x60;pending&#x60;, &#x60;expired&#x60;, &#x60;approved&#x60; or &#x60;denied&#x60;. | [optional] 
 **order** | **ChallengeEnumListOrders**| The desired sort order of the Challenges list. One of &#x60;asc&#x60; or &#x60;desc&#x60; for ascending and descending respectively. Defaults to &#x60;asc&#x60;. | [optional] 
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListChallengeResponse**](ListChallengeResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_entity**
> ListEntityResponse list_entity(service_sid, page_size=page_size)



Retrieve a list of all Entities for a Service.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_entity(service_sid, page_size=page_size)
        print("The response of DefaultApi->list_entity:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_entity: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListEntityResponse**](ListEntityResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_factor**
> ListFactorResponse list_factor(service_sid, identity, page_size=page_size)



Retrieve a list of all Factors for an Entity.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | Customer unique identity for the Entity owner of the Factors. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_factor(service_sid, identity, page_size=page_size)
        print("The response of DefaultApi->list_factor:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_factor: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| Customer unique identity for the Entity owner of the Factors. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListFactorResponse**](ListFactorResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_messaging_configuration**
> ListMessagingConfigurationResponse list_messaging_configuration(service_sid, page_size=page_size)



Retrieve a list of all Messaging Configurations for a Service.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) that the resource is associated with.
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_messaging_configuration(service_sid, page_size=page_size)
        print("The response of DefaultApi->list_messaging_configuration:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_messaging_configuration: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) that the resource is associated with. | 
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListMessagingConfigurationResponse**](ListMessagingConfigurationResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_rate_limit**
> ListRateLimitResponse list_rate_limit(service_sid, page_size=page_size)



Retrieve a list of all Rate Limits for a service.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with.
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_rate_limit(service_sid, page_size=page_size)
        print("The response of DefaultApi->list_rate_limit:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_rate_limit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with. | 
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListRateLimitResponse**](ListRateLimitResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_service**
> ListServiceResponse list_service(page_size=page_size)



Retrieve a list of all Verification Services for an account.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_service(page_size=page_size)
        print("The response of DefaultApi->list_service:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_service: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListServiceResponse**](ListServiceResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_verification_attempt**
> ListVerificationAttemptResponse list_verification_attempt(date_created_after=date_created_after, date_created_before=date_created_before, channel_data_to=channel_data_to, country=country, channel=channel, verify_service_sid=verify_service_sid, verification_sid=verification_sid, status=status, page_size=page_size)



List all the verification attempts for a given Account.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    date_created_after = '2013-10-20T19:20:30+01:00' # datetime | Datetime filter used to query Verification Attempts created after this datetime. Given as GMT in RFC 2822 format. (optional)
    date_created_before = '2013-10-20T19:20:30+01:00' # datetime | Datetime filter used to query Verification Attempts created before this datetime. Given as GMT in RFC 2822 format. (optional)
    channel_data_to = 'channel_data_to_example' # str | Destination of a verification. It is phone number in E.164 format. (optional)
    country = 'country_example' # str | Filter used to query Verification Attempts sent to the specified destination country. (optional)
    channel = py_twilio_verify.VerificationAttemptEnumChannels() # VerificationAttemptEnumChannels | Filter used to query Verification Attempts by communication channel. Valid values are `SMS` and `CALL` (optional)
    verify_service_sid = 'verify_service_sid_example' # str | Filter used to query Verification Attempts by verify service. Only attempts of the provided SID will be returned. (optional)
    verification_sid = 'verification_sid_example' # str | Filter used to return all the Verification Attempts of a single verification. Only attempts of the provided verification SID will be returned. (optional)
    status = py_twilio_verify.VerificationAttemptEnumConversionStatus() # VerificationAttemptEnumConversionStatus | Filter used to query Verification Attempts by conversion status. Valid values are `UNCONVERTED`, for attempts that were not converted, and `CONVERTED`, for attempts that were confirmed. (optional)
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_verification_attempt(date_created_after=date_created_after, date_created_before=date_created_before, channel_data_to=channel_data_to, country=country, channel=channel, verify_service_sid=verify_service_sid, verification_sid=verification_sid, status=status, page_size=page_size)
        print("The response of DefaultApi->list_verification_attempt:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_verification_attempt: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date_created_after** | **datetime**| Datetime filter used to query Verification Attempts created after this datetime. Given as GMT in RFC 2822 format. | [optional] 
 **date_created_before** | **datetime**| Datetime filter used to query Verification Attempts created before this datetime. Given as GMT in RFC 2822 format. | [optional] 
 **channel_data_to** | **str**| Destination of a verification. It is phone number in E.164 format. | [optional] 
 **country** | **str**| Filter used to query Verification Attempts sent to the specified destination country. | [optional] 
 **channel** | **VerificationAttemptEnumChannels**| Filter used to query Verification Attempts by communication channel. Valid values are &#x60;SMS&#x60; and &#x60;CALL&#x60; | [optional] 
 **verify_service_sid** | **str**| Filter used to query Verification Attempts by verify service. Only attempts of the provided SID will be returned. | [optional] 
 **verification_sid** | **str**| Filter used to return all the Verification Attempts of a single verification. Only attempts of the provided verification SID will be returned. | [optional] 
 **status** | **VerificationAttemptEnumConversionStatus**| Filter used to query Verification Attempts by conversion status. Valid values are &#x60;UNCONVERTED&#x60;, for attempts that were not converted, and &#x60;CONVERTED&#x60;, for attempts that were confirmed. | [optional] 
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListVerificationAttemptResponse**](ListVerificationAttemptResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_verification_template**
> ListVerificationTemplateResponse list_verification_template(friendly_name=friendly_name, page_size=page_size)



List all the available templates for a given Account.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    friendly_name = 'friendly_name_example' # str | String filter used to query templates with a given friendly name (optional)
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_verification_template(friendly_name=friendly_name, page_size=page_size)
        print("The response of DefaultApi->list_verification_template:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_verification_template: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **friendly_name** | **str**| String filter used to query templates with a given friendly name | [optional] 
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListVerificationTemplateResponse**](ListVerificationTemplateResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **list_webhook**
> ListWebhookResponse list_webhook(service_sid, page_size=page_size)



Retrieve a list of all Webhooks for a Service.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    page_size = 56 # int | How many resources to return in each list page. The default is 50, and the maximum is 1000. (optional)

    try:
        api_response = api_instance.list_webhook(service_sid, page_size=page_size)
        print("The response of DefaultApi->list_webhook:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->list_webhook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **page_size** | **int**| How many resources to return in each list page. The default is 50, and the maximum is 1000. | [optional] 

### Return type

[**ListWebhookResponse**](ListWebhookResponse.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_bucket**
> VerifyV2ServiceRateLimitBucket update_bucket(service_sid, rate_limit_sid, sid, max=max, interval=interval)



Update a specific Bucket.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with.
    rate_limit_sid = 'rate_limit_sid_example' # str | The Twilio-provided string that uniquely identifies the Rate Limit resource.
    sid = 'sid_example' # str | A 34 character string that uniquely identifies this Bucket.
    max = 56 # int | Maximum number of requests permitted in during the interval. (optional)
    interval = 56 # int | Number of seconds that the rate limit will be enforced over. (optional)

    try:
        api_response = api_instance.update_bucket(service_sid, rate_limit_sid, sid, max=max, interval=interval)
        print("The response of DefaultApi->update_bucket:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->update_bucket: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with. | 
 **rate_limit_sid** | **str**| The Twilio-provided string that uniquely identifies the Rate Limit resource. | 
 **sid** | **str**| A 34 character string that uniquely identifies this Bucket. | 
 **max** | **int**| Maximum number of requests permitted in during the interval. | [optional] 
 **interval** | **int**| Number of seconds that the rate limit will be enforced over. | [optional] 

### Return type

[**VerifyV2ServiceRateLimitBucket**](VerifyV2ServiceRateLimitBucket.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_challenge**
> VerifyV2ServiceEntityChallenge update_challenge(service_sid, identity, sid, auth_payload=auth_payload, metadata=metadata)



Verify a specific Challenge.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | Customer unique identity for the Entity owner of the Challenge. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.
    sid = 'sid_example' # str | A 34 character string that uniquely identifies this Challenge.
    auth_payload = 'auth_payload_example' # str | The optional payload needed to verify the Challenge. E.g., a TOTP would use the numeric code. For `TOTP` this value must be between 3 and 8 characters long. For `Push` this value can be up to 5456 characters in length (optional)
    metadata = None # object | Custom metadata associated with the challenge. This is added by the Device/SDK directly to allow for the inclusion of device information. It must be a stringified JSON with only strings values eg. `{\\\"os\\\": \\\"Android\\\"}`. Can be up to 1024 characters in length. (optional)

    try:
        api_response = api_instance.update_challenge(service_sid, identity, sid, auth_payload=auth_payload, metadata=metadata)
        print("The response of DefaultApi->update_challenge:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->update_challenge: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| Customer unique identity for the Entity owner of the Challenge. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 
 **sid** | **str**| A 34 character string that uniquely identifies this Challenge. | 
 **auth_payload** | **str**| The optional payload needed to verify the Challenge. E.g., a TOTP would use the numeric code. For &#x60;TOTP&#x60; this value must be between 3 and 8 characters long. For &#x60;Push&#x60; this value can be up to 5456 characters in length | [optional] 
 **metadata** | [**object**](object.md)| Custom metadata associated with the challenge. This is added by the Device/SDK directly to allow for the inclusion of device information. It must be a stringified JSON with only strings values eg. &#x60;{\\\&quot;os\\\&quot;: \\\&quot;Android\\\&quot;}&#x60;. Can be up to 1024 characters in length. | [optional] 

### Return type

[**VerifyV2ServiceEntityChallenge**](VerifyV2ServiceEntityChallenge.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_factor**
> VerifyV2ServiceEntityFactor update_factor(service_sid, identity, sid, auth_payload=auth_payload, friendly_name=friendly_name, config_notification_token=config_notification_token, config_sdk_version=config_sdk_version, config_time_step=config_time_step, config_skew=config_skew, config_code_length=config_code_length, config_alg=config_alg, config_notification_platform=config_notification_platform)



Update a specific Factor. This endpoint can be used to Verify a Factor if passed an `AuthPayload` param.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    identity = 'identity_example' # str | Customer unique identity for the Entity owner of the Factor. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user's UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters.
    sid = 'sid_example' # str | A 34 character string that uniquely identifies this Factor.
    auth_payload = 'auth_payload_example' # str | The optional payload needed to verify the Factor for the first time. E.g. for a TOTP, the numeric code. (optional)
    friendly_name = 'friendly_name_example' # str | The new friendly name of this Factor. It can be up to 64 characters. (optional)
    config_notification_token = 'config_notification_token_example' # str | For APN, the device token. For FCM, the registration token. It is used to send the push notifications. Required when `factor_type` is `push`. If specified, this value must be between 32 and 255 characters long. (optional)
    config_sdk_version = 'config_sdk_version_example' # str | The Verify Push SDK version used to configure the factor (optional)
    config_time_step = 56 # int | Defines how often, in seconds, are TOTP codes generated. i.e, a new TOTP code is generated every time_step seconds. Must be between 20 and 60 seconds, inclusive (optional)
    config_skew = 56 # int | The number of time-steps, past and future, that are valid for validation of TOTP codes. Must be between 0 and 2, inclusive (optional)
    config_code_length = 56 # int | Number of digits for generated TOTP codes. Must be between 3 and 8, inclusive (optional)
    config_alg = py_twilio_verify.FactorEnumTotpAlgorithms() # FactorEnumTotpAlgorithms |  (optional)
    config_notification_platform = 'config_notification_platform_example' # str | The transport technology used to generate the Notification Token. Can be `apn`, `fcm` or `none`.  Required when `factor_type` is `push`. (optional)

    try:
        api_response = api_instance.update_factor(service_sid, identity, sid, auth_payload=auth_payload, friendly_name=friendly_name, config_notification_token=config_notification_token, config_sdk_version=config_sdk_version, config_time_step=config_time_step, config_skew=config_skew, config_code_length=config_code_length, config_alg=config_alg, config_notification_platform=config_notification_platform)
        print("The response of DefaultApi->update_factor:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->update_factor: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **identity** | **str**| Customer unique identity for the Entity owner of the Factor. This identifier should be immutable, not PII, length between 8 and 64 characters, and generated by your external system, such as your user&#39;s UUID, GUID, or SID. It can only contain dash (-) separated alphanumeric characters. | 
 **sid** | **str**| A 34 character string that uniquely identifies this Factor. | 
 **auth_payload** | **str**| The optional payload needed to verify the Factor for the first time. E.g. for a TOTP, the numeric code. | [optional] 
 **friendly_name** | **str**| The new friendly name of this Factor. It can be up to 64 characters. | [optional] 
 **config_notification_token** | **str**| For APN, the device token. For FCM, the registration token. It is used to send the push notifications. Required when &#x60;factor_type&#x60; is &#x60;push&#x60;. If specified, this value must be between 32 and 255 characters long. | [optional] 
 **config_sdk_version** | **str**| The Verify Push SDK version used to configure the factor | [optional] 
 **config_time_step** | **int**| Defines how often, in seconds, are TOTP codes generated. i.e, a new TOTP code is generated every time_step seconds. Must be between 20 and 60 seconds, inclusive | [optional] 
 **config_skew** | **int**| The number of time-steps, past and future, that are valid for validation of TOTP codes. Must be between 0 and 2, inclusive | [optional] 
 **config_code_length** | **int**| Number of digits for generated TOTP codes. Must be between 3 and 8, inclusive | [optional] 
 **config_alg** | **FactorEnumTotpAlgorithms**|  | [optional] 
 **config_notification_platform** | **str**| The transport technology used to generate the Notification Token. Can be &#x60;apn&#x60;, &#x60;fcm&#x60; or &#x60;none&#x60;.  Required when &#x60;factor_type&#x60; is &#x60;push&#x60;. | [optional] 

### Return type

[**VerifyV2ServiceEntityFactor**](VerifyV2ServiceEntityFactor.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_messaging_configuration**
> VerifyV2ServiceMessagingConfiguration update_messaging_configuration(service_sid, country, messaging_service_sid)



Update a specific MessagingConfiguration

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) that the resource is associated with.
    country = 'country_example' # str | The [ISO-3166-1](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) country code of the country this configuration will be applied to. If this is a global configuration, Country will take the value `all`.
    messaging_service_sid = 'messaging_service_sid_example' # str | The SID of the [Messaging Service](https://www.twilio.com/docs/sms/services/api) to be used to send SMS to the country of this configuration.

    try:
        api_response = api_instance.update_messaging_configuration(service_sid, country, messaging_service_sid)
        print("The response of DefaultApi->update_messaging_configuration:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->update_messaging_configuration: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) that the resource is associated with. | 
 **country** | **str**| The [ISO-3166-1](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2) country code of the country this configuration will be applied to. If this is a global configuration, Country will take the value &#x60;all&#x60;. | 
 **messaging_service_sid** | **str**| The SID of the [Messaging Service](https://www.twilio.com/docs/sms/services/api) to be used to send SMS to the country of this configuration. | 

### Return type

[**VerifyV2ServiceMessagingConfiguration**](VerifyV2ServiceMessagingConfiguration.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_rate_limit**
> VerifyV2ServiceRateLimit update_rate_limit(service_sid, sid, description=description)



Update a specific Rate Limit.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with.
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the Rate Limit resource to fetch.
    description = 'description_example' # str | Description of this Rate Limit (optional)

    try:
        api_response = api_instance.update_rate_limit(service_sid, sid, description=description)
        print("The response of DefaultApi->update_rate_limit:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->update_rate_limit: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the [Service](https://www.twilio.com/docs/verify/api/service) the resource is associated with. | 
 **sid** | **str**| The Twilio-provided string that uniquely identifies the Rate Limit resource to fetch. | 
 **description** | **str**| Description of this Rate Limit | [optional] 

### Return type

[**VerifyV2ServiceRateLimit**](VerifyV2ServiceRateLimit.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_service**
> VerifyV2Service update_service(sid, friendly_name=friendly_name, code_length=code_length, lookup_enabled=lookup_enabled, skip_sms_to_landlines=skip_sms_to_landlines, dtmf_input_required=dtmf_input_required, tts_name=tts_name, psd2_enabled=psd2_enabled, do_not_share_warning_enabled=do_not_share_warning_enabled, custom_code_enabled=custom_code_enabled, push_include_date=push_include_date, push_apn_credential_sid=push_apn_credential_sid, push_fcm_credential_sid=push_fcm_credential_sid, totp_issuer=totp_issuer, totp_time_step=totp_time_step, totp_code_length=totp_code_length, totp_skew=totp_skew, default_template_sid=default_template_sid)



Update a specific Verification Service.

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the Service resource to update.
    friendly_name = 'friendly_name_example' # str | A descriptive string that you create to describe the verification service. It can be up to 30 characters long. **This value should not contain PII.** (optional)
    code_length = 56 # int | The length of the verification code to generate. Must be an integer value between 4 and 10, inclusive. (optional)
    lookup_enabled = True # bool | Whether to perform a lookup with each verification started and return info about the phone number. (optional)
    skip_sms_to_landlines = True # bool | Whether to skip sending SMS verifications to landlines. Requires `lookup_enabled`. (optional)
    dtmf_input_required = True # bool | Whether to ask the user to press a number before delivering the verify code in a phone call. (optional)
    tts_name = 'tts_name_example' # str | The name of an alternative text-to-speech service to use in phone calls. Applies only to TTS languages. (optional)
    psd2_enabled = True # bool | Whether to pass PSD2 transaction parameters when starting a verification. (optional)
    do_not_share_warning_enabled = True # bool | Whether to add a privacy warning at the end of an SMS. **Disabled by default and applies only for SMS.** (optional)
    custom_code_enabled = True # bool | Whether to allow sending verifications with a custom code instead of a randomly generated one. Not available for all customers. (optional)
    push_include_date = True # bool | Optional configuration for the Push factors. If true, include the date in the Challenge's response. Otherwise, the date is omitted from the response. See [Challenge](https://www.twilio.com/docs/verify/api/challenge) resource’s details parameter for more info. Default: false. **Deprecated** do not use this parameter. (optional)
    push_apn_credential_sid = 'push_apn_credential_sid_example' # str | Optional configuration for the Push factors. Set the APN Credential for this service. This will allow to send push notifications to iOS devices. See [Credential Resource](https://www.twilio.com/docs/notify/api/credential-resource) (optional)
    push_fcm_credential_sid = 'push_fcm_credential_sid_example' # str | Optional configuration for the Push factors. Set the FCM Credential for this service. This will allow to send push notifications to Android devices. See [Credential Resource](https://www.twilio.com/docs/notify/api/credential-resource) (optional)
    totp_issuer = 'totp_issuer_example' # str | Optional configuration for the TOTP factors. Set TOTP Issuer for this service. This will allow to configure the issuer of the TOTP URI. (optional)
    totp_time_step = 56 # int | Optional configuration for the TOTP factors. Defines how often, in seconds, are TOTP codes generated. i.e, a new TOTP code is generated every time_step seconds. Must be between 20 and 60 seconds, inclusive. Defaults to 30 seconds (optional)
    totp_code_length = 56 # int | Optional configuration for the TOTP factors. Number of digits for generated TOTP codes. Must be between 3 and 8, inclusive. Defaults to 6 (optional)
    totp_skew = 56 # int | Optional configuration for the TOTP factors. The number of time-steps, past and future, that are valid for validation of TOTP codes. Must be between 0 and 2, inclusive. Defaults to 1 (optional)
    default_template_sid = 'default_template_sid_example' # str | The default message [template](https://www.twilio.com/docs/verify/api/templates). Will be used for all SMS verifications unless explicitly overriden. SMS channel only. (optional)

    try:
        api_response = api_instance.update_service(sid, friendly_name=friendly_name, code_length=code_length, lookup_enabled=lookup_enabled, skip_sms_to_landlines=skip_sms_to_landlines, dtmf_input_required=dtmf_input_required, tts_name=tts_name, psd2_enabled=psd2_enabled, do_not_share_warning_enabled=do_not_share_warning_enabled, custom_code_enabled=custom_code_enabled, push_include_date=push_include_date, push_apn_credential_sid=push_apn_credential_sid, push_fcm_credential_sid=push_fcm_credential_sid, totp_issuer=totp_issuer, totp_time_step=totp_time_step, totp_code_length=totp_code_length, totp_skew=totp_skew, default_template_sid=default_template_sid)
        print("The response of DefaultApi->update_service:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->update_service: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **sid** | **str**| The Twilio-provided string that uniquely identifies the Service resource to update. | 
 **friendly_name** | **str**| A descriptive string that you create to describe the verification service. It can be up to 30 characters long. **This value should not contain PII.** | [optional] 
 **code_length** | **int**| The length of the verification code to generate. Must be an integer value between 4 and 10, inclusive. | [optional] 
 **lookup_enabled** | **bool**| Whether to perform a lookup with each verification started and return info about the phone number. | [optional] 
 **skip_sms_to_landlines** | **bool**| Whether to skip sending SMS verifications to landlines. Requires &#x60;lookup_enabled&#x60;. | [optional] 
 **dtmf_input_required** | **bool**| Whether to ask the user to press a number before delivering the verify code in a phone call. | [optional] 
 **tts_name** | **str**| The name of an alternative text-to-speech service to use in phone calls. Applies only to TTS languages. | [optional] 
 **psd2_enabled** | **bool**| Whether to pass PSD2 transaction parameters when starting a verification. | [optional] 
 **do_not_share_warning_enabled** | **bool**| Whether to add a privacy warning at the end of an SMS. **Disabled by default and applies only for SMS.** | [optional] 
 **custom_code_enabled** | **bool**| Whether to allow sending verifications with a custom code instead of a randomly generated one. Not available for all customers. | [optional] 
 **push_include_date** | **bool**| Optional configuration for the Push factors. If true, include the date in the Challenge&#39;s response. Otherwise, the date is omitted from the response. See [Challenge](https://www.twilio.com/docs/verify/api/challenge) resource’s details parameter for more info. Default: false. **Deprecated** do not use this parameter. | [optional] 
 **push_apn_credential_sid** | **str**| Optional configuration for the Push factors. Set the APN Credential for this service. This will allow to send push notifications to iOS devices. See [Credential Resource](https://www.twilio.com/docs/notify/api/credential-resource) | [optional] 
 **push_fcm_credential_sid** | **str**| Optional configuration for the Push factors. Set the FCM Credential for this service. This will allow to send push notifications to Android devices. See [Credential Resource](https://www.twilio.com/docs/notify/api/credential-resource) | [optional] 
 **totp_issuer** | **str**| Optional configuration for the TOTP factors. Set TOTP Issuer for this service. This will allow to configure the issuer of the TOTP URI. | [optional] 
 **totp_time_step** | **int**| Optional configuration for the TOTP factors. Defines how often, in seconds, are TOTP codes generated. i.e, a new TOTP code is generated every time_step seconds. Must be between 20 and 60 seconds, inclusive. Defaults to 30 seconds | [optional] 
 **totp_code_length** | **int**| Optional configuration for the TOTP factors. Number of digits for generated TOTP codes. Must be between 3 and 8, inclusive. Defaults to 6 | [optional] 
 **totp_skew** | **int**| Optional configuration for the TOTP factors. The number of time-steps, past and future, that are valid for validation of TOTP codes. Must be between 0 and 2, inclusive. Defaults to 1 | [optional] 
 **default_template_sid** | **str**| The default message [template](https://www.twilio.com/docs/verify/api/templates). Will be used for all SMS verifications unless explicitly overriden. SMS channel only. | [optional] 

### Return type

[**VerifyV2Service**](VerifyV2Service.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_verification**
> VerifyV2ServiceVerification update_verification(service_sid, sid, status)



Update a Verification status

### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The SID of the verification [Service](https://www.twilio.com/docs/verify/api/service) to update the resource from.
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the Verification resource to update.
    status = py_twilio_verify.VerificationEnumStatus() # VerificationEnumStatus | 

    try:
        api_response = api_instance.update_verification(service_sid, sid, status)
        print("The response of DefaultApi->update_verification:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->update_verification: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The SID of the verification [Service](https://www.twilio.com/docs/verify/api/service) to update the resource from. | 
 **sid** | **str**| The Twilio-provided string that uniquely identifies the Verification resource to update. | 
 **status** | **VerificationEnumStatus**|  | 

### Return type

[**VerifyV2ServiceVerification**](VerifyV2ServiceVerification.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_webhook**
> VerifyV2ServiceWebhook update_webhook(service_sid, sid, friendly_name=friendly_name, event_types=event_types, webhook_url=webhook_url, status=status, version=version)





### Example

* Basic Authentication (accountSid_authToken):
```python
from __future__ import print_function
import time
import os
import py_twilio_verify
from py_twilio_verify.rest import ApiException
from pprint import pprint
# Defining the host is optional and defaults to https://verify.twilio.com
# See configuration.py for a list of all supported configuration parameters.
configuration = py_twilio_verify.Configuration(
    host = "https://verify.twilio.com"
)

# The client must configure the authentication and authorization parameters
# in accordance with the API server security policy.
# Examples for each auth method are provided below, use the example that
# satisfies your auth use case.

# Configure HTTP basic authorization: accountSid_authToken
configuration = py_twilio_verify.Configuration(
    username = os.environ["USERNAME"],
    password = os.environ["PASSWORD"]
)

# Enter a context with an instance of the API client
with py_twilio_verify.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = py_twilio_verify.DefaultApi(api_client)
    service_sid = 'service_sid_example' # str | The unique SID identifier of the Service.
    sid = 'sid_example' # str | The Twilio-provided string that uniquely identifies the Webhook resource to update.
    friendly_name = 'friendly_name_example' # str | The string that you assigned to describe the webhook. **This value should not contain PII.** (optional)
    event_types = ['event_types_example'] # List[str] | The array of events that this Webhook is subscribed to. Possible event types: `*, factor.deleted, factor.created, factor.verified, challenge.approved, challenge.denied` (optional)
    webhook_url = 'webhook_url_example' # str | The URL associated with this Webhook. (optional)
    status = py_twilio_verify.WebhookEnumStatus() # WebhookEnumStatus |  (optional)
    version = py_twilio_verify.WebhookEnumVersion() # WebhookEnumVersion |  (optional)

    try:
        api_response = api_instance.update_webhook(service_sid, sid, friendly_name=friendly_name, event_types=event_types, webhook_url=webhook_url, status=status, version=version)
        print("The response of DefaultApi->update_webhook:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->update_webhook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **service_sid** | **str**| The unique SID identifier of the Service. | 
 **sid** | **str**| The Twilio-provided string that uniquely identifies the Webhook resource to update. | 
 **friendly_name** | **str**| The string that you assigned to describe the webhook. **This value should not contain PII.** | [optional] 
 **event_types** | [**List[str]**](str.md)| The array of events that this Webhook is subscribed to. Possible event types: &#x60;*, factor.deleted, factor.created, factor.verified, challenge.approved, challenge.denied&#x60; | [optional] 
 **webhook_url** | **str**| The URL associated with this Webhook. | [optional] 
 **status** | **WebhookEnumStatus**|  | [optional] 
 **version** | **WebhookEnumVersion**|  | [optional] 

### Return type

[**VerifyV2ServiceWebhook**](VerifyV2ServiceWebhook.md)

### Authorization

[accountSid_authToken](../README.md#accountSid_authToken)

### HTTP request headers

 - **Content-Type**: application/x-www-form-urlencoded
 - **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

