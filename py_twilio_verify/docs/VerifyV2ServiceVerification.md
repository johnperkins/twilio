# VerifyV2ServiceVerification


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | The unique string that identifies the resource | [optional] 
**service_sid** | **str** | The SID of the Service that the resource is associated with | [optional] 
**account_sid** | **str** | The SID of the Account that created the resource | [optional] 
**to** | **str** | The phone number or email being verified | [optional] 
**channel** | [**VerificationEnumChannel**](VerificationEnumChannel.md) |  | [optional] 
**status** | **str** | The status of the verification resource | [optional] 
**valid** | **bool** | Whether the verification was successful | [optional] 
**lookup** | **object** | Information about the phone number being verified | [optional] 
**amount** | **str** | The amount of the associated PSD2 compliant transaction. | [optional] 
**payee** | **str** | The payee of the associated PSD2 compliant transaction | [optional] 
**send_code_attempts** | **List[object]** | An array of verification attempt objects. | [optional] 
**date_created** | **datetime** | The RFC 2822 date and time in GMT when the resource was created | [optional] 
**date_updated** | **datetime** | The RFC 2822 date and time in GMT when the resource was last updated | [optional] 
**sna** | **object** | The set of fields used for a silent network auth (&#x60;sna&#x60;) verification | [optional] 
**url** | **str** | The absolute URL of the Verification resource | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_service_verification import VerifyV2ServiceVerification

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2ServiceVerification from a JSON string
verify_v2_service_verification_instance = VerifyV2ServiceVerification.from_json(json)
# print the JSON string representation of the object
print VerifyV2ServiceVerification.to_json()

# convert the object into a dict
verify_v2_service_verification_dict = verify_v2_service_verification_instance.to_dict()
# create an instance of VerifyV2ServiceVerification from a dict
verify_v2_service_verification_form_dict = verify_v2_service_verification.from_dict(verify_v2_service_verification_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


