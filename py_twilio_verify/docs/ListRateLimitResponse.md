# ListRateLimitResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rate_limits** | [**List[VerifyV2ServiceRateLimit]**](VerifyV2ServiceRateLimit.md) |  | [optional] 
**meta** | [**ListBucketResponseMeta**](ListBucketResponseMeta.md) |  | [optional] 

## Example

```python
from py_twilio_verify.models.list_rate_limit_response import ListRateLimitResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ListRateLimitResponse from a JSON string
list_rate_limit_response_instance = ListRateLimitResponse.from_json(json)
# print the JSON string representation of the object
print ListRateLimitResponse.to_json()

# convert the object into a dict
list_rate_limit_response_dict = list_rate_limit_response_instance.to_dict()
# create an instance of ListRateLimitResponse from a dict
list_rate_limit_response_form_dict = list_rate_limit_response.from_dict(list_rate_limit_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


