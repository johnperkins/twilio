# ListChallengeResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**challenges** | [**List[VerifyV2ServiceEntityChallenge]**](VerifyV2ServiceEntityChallenge.md) |  | [optional] 
**meta** | [**ListBucketResponseMeta**](ListBucketResponseMeta.md) |  | [optional] 

## Example

```python
from py_twilio_verify.models.list_challenge_response import ListChallengeResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ListChallengeResponse from a JSON string
list_challenge_response_instance = ListChallengeResponse.from_json(json)
# print the JSON string representation of the object
print ListChallengeResponse.to_json()

# convert the object into a dict
list_challenge_response_dict = list_challenge_response_instance.to_dict()
# create an instance of ListChallengeResponse from a dict
list_challenge_response_form_dict = list_challenge_response.from_dict(list_challenge_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


