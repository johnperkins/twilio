# VerifyV2ServiceEntityFactor


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | A string that uniquely identifies this Factor. | [optional] 
**account_sid** | **str** | Account Sid. | [optional] 
**service_sid** | **str** | Service Sid. | [optional] 
**entity_sid** | **str** | Entity Sid. | [optional] 
**identity** | **str** | Unique external identifier of the Entity | [optional] 
**date_created** | **datetime** | The date this Factor was created | [optional] 
**date_updated** | **datetime** | The date this Factor was updated | [optional] 
**friendly_name** | **str** | A human readable description of this resource. | [optional] 
**status** | [**FactorEnumFactorStatuses**](FactorEnumFactorStatuses.md) |  | [optional] 
**factor_type** | [**FactorEnumFactorTypes**](FactorEnumFactorTypes.md) |  | [optional] 
**config** | **object** | Configurations for a &#x60;factor_type&#x60;. | [optional] 
**metadata** | **object** | Metadata of the factor. | [optional] 
**url** | **str** | The URL of this resource. | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_service_entity_factor import VerifyV2ServiceEntityFactor

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2ServiceEntityFactor from a JSON string
verify_v2_service_entity_factor_instance = VerifyV2ServiceEntityFactor.from_json(json)
# print the JSON string representation of the object
print VerifyV2ServiceEntityFactor.to_json()

# convert the object into a dict
verify_v2_service_entity_factor_dict = verify_v2_service_entity_factor_instance.to_dict()
# create an instance of VerifyV2ServiceEntityFactor from a dict
verify_v2_service_entity_factor_form_dict = verify_v2_service_entity_factor.from_dict(verify_v2_service_entity_factor_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


