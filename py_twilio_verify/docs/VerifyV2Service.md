# VerifyV2Service


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | The unique string that identifies the resource | [optional] 
**account_sid** | **str** | The SID of the Account that created the resource | [optional] 
**friendly_name** | **str** | The string that you assigned to describe the verification service | [optional] 
**code_length** | **int** | The length of the verification code | [optional] 
**lookup_enabled** | **bool** | Whether to perform a lookup with each verification | [optional] 
**psd2_enabled** | **bool** | Whether to pass PSD2 transaction parameters when starting a verification | [optional] 
**skip_sms_to_landlines** | **bool** | Whether to skip sending SMS verifications to landlines | [optional] 
**dtmf_input_required** | **bool** | Whether to ask the user to press a number before delivering the verify code in a phone call | [optional] 
**tts_name** | **str** | The name of an alternative text-to-speech service to use in phone calls | [optional] 
**do_not_share_warning_enabled** | **bool** | Whether to add a security warning at the end of an SMS. | [optional] 
**custom_code_enabled** | **bool** | Whether to allow sending verifications with a custom code. | [optional] 
**push** | **object** | The service level configuration of factor push type. | [optional] 
**totp** | **object** | The service level configuration of factor TOTP type. | [optional] 
**default_template_sid** | **str** |  | [optional] 
**date_created** | **datetime** | The RFC 2822 date and time in GMT when the resource was created | [optional] 
**date_updated** | **datetime** | The RFC 2822 date and time in GMT when the resource was last updated | [optional] 
**url** | **str** | The absolute URL of the resource | [optional] 
**links** | **object** | The URLs of related resources | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_service import VerifyV2Service

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2Service from a JSON string
verify_v2_service_instance = VerifyV2Service.from_json(json)
# print the JSON string representation of the object
print VerifyV2Service.to_json()

# convert the object into a dict
verify_v2_service_dict = verify_v2_service_instance.to_dict()
# create an instance of VerifyV2Service from a dict
verify_v2_service_form_dict = verify_v2_service.from_dict(verify_v2_service_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


