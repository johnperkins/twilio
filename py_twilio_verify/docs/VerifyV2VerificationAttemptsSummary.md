# VerifyV2VerificationAttemptsSummary


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total_attempts** | **int** | Total of attempts made. | [optional] 
**total_converted** | **int** | Total of attempts confirmed by the end user. | [optional] 
**total_unconverted** | **int** | Total of attempts made that were not confirmed by the end user. | [optional] 
**conversion_rate_percentage** | **float** | Percentage of the confirmed messages over the total. | [optional] 
**url** | **str** |  | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_verification_attempts_summary import VerifyV2VerificationAttemptsSummary

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2VerificationAttemptsSummary from a JSON string
verify_v2_verification_attempts_summary_instance = VerifyV2VerificationAttemptsSummary.from_json(json)
# print the JSON string representation of the object
print VerifyV2VerificationAttemptsSummary.to_json()

# convert the object into a dict
verify_v2_verification_attempts_summary_dict = verify_v2_verification_attempts_summary_instance.to_dict()
# create an instance of VerifyV2VerificationAttemptsSummary from a dict
verify_v2_verification_attempts_summary_form_dict = verify_v2_verification_attempts_summary.from_dict(verify_v2_verification_attempts_summary_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


