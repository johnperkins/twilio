# ListVerificationTemplateResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**templates** | [**List[VerifyV2VerificationTemplate]**](VerifyV2VerificationTemplate.md) |  | [optional] 
**meta** | [**ListBucketResponseMeta**](ListBucketResponseMeta.md) |  | [optional] 

## Example

```python
from py_twilio_verify.models.list_verification_template_response import ListVerificationTemplateResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ListVerificationTemplateResponse from a JSON string
list_verification_template_response_instance = ListVerificationTemplateResponse.from_json(json)
# print the JSON string representation of the object
print ListVerificationTemplateResponse.to_json()

# convert the object into a dict
list_verification_template_response_dict = list_verification_template_response_instance.to_dict()
# create an instance of ListVerificationTemplateResponse from a dict
list_verification_template_response_form_dict = list_verification_template_response.from_dict(list_verification_template_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


