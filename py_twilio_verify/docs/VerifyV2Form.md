# VerifyV2Form


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**form_type** | [**FormEnumFormTypes**](FormEnumFormTypes.md) |  | [optional] 
**forms** | **object** | Object that contains the available forms for this type. | [optional] 
**form_meta** | **object** | Additional information for the available forms for this type. | [optional] 
**url** | **str** | The URL to access the forms for this type. | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_form import VerifyV2Form

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2Form from a JSON string
verify_v2_form_instance = VerifyV2Form.from_json(json)
# print the JSON string representation of the object
print VerifyV2Form.to_json()

# convert the object into a dict
verify_v2_form_dict = verify_v2_form_instance.to_dict()
# create an instance of VerifyV2Form from a dict
verify_v2_form_form_dict = verify_v2_form.from_dict(verify_v2_form_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


