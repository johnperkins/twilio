# VerifyV2Safelist


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | The unique string that identifies the resource. | [optional] 
**phone_number** | **str** | The phone number in SafeList. | [optional] 
**url** | **str** | The absolute URL of the SafeList resource. | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_safelist import VerifyV2Safelist

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2Safelist from a JSON string
verify_v2_safelist_instance = VerifyV2Safelist.from_json(json)
# print the JSON string representation of the object
print VerifyV2Safelist.to_json()

# convert the object into a dict
verify_v2_safelist_dict = verify_v2_safelist_instance.to_dict()
# create an instance of VerifyV2Safelist from a dict
verify_v2_safelist_form_dict = verify_v2_safelist.from_dict(verify_v2_safelist_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


