# ListMessagingConfigurationResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**messaging_configurations** | [**List[VerifyV2ServiceMessagingConfiguration]**](VerifyV2ServiceMessagingConfiguration.md) |  | [optional] 
**meta** | [**ListBucketResponseMeta**](ListBucketResponseMeta.md) |  | [optional] 

## Example

```python
from py_twilio_verify.models.list_messaging_configuration_response import ListMessagingConfigurationResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ListMessagingConfigurationResponse from a JSON string
list_messaging_configuration_response_instance = ListMessagingConfigurationResponse.from_json(json)
# print the JSON string representation of the object
print ListMessagingConfigurationResponse.to_json()

# convert the object into a dict
list_messaging_configuration_response_dict = list_messaging_configuration_response_instance.to_dict()
# create an instance of ListMessagingConfigurationResponse from a dict
list_messaging_configuration_response_form_dict = list_messaging_configuration_response.from_dict(list_messaging_configuration_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


