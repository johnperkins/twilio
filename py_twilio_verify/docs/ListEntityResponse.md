# ListEntityResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**entities** | [**List[VerifyV2ServiceEntity]**](VerifyV2ServiceEntity.md) |  | [optional] 
**meta** | [**ListBucketResponseMeta**](ListBucketResponseMeta.md) |  | [optional] 

## Example

```python
from py_twilio_verify.models.list_entity_response import ListEntityResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ListEntityResponse from a JSON string
list_entity_response_instance = ListEntityResponse.from_json(json)
# print the JSON string representation of the object
print ListEntityResponse.to_json()

# convert the object into a dict
list_entity_response_dict = list_entity_response_instance.to_dict()
# create an instance of ListEntityResponse from a dict
list_entity_response_form_dict = list_entity_response.from_dict(list_entity_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


