# VerifyV2ServiceEntity


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | A string that uniquely identifies this Entity. | [optional] 
**identity** | **str** | Unique external identifier of the Entity | [optional] 
**account_sid** | **str** | Account Sid. | [optional] 
**service_sid** | **str** | Service Sid. | [optional] 
**date_created** | **datetime** | The date this Entity was created | [optional] 
**date_updated** | **datetime** | The date this Entity was updated | [optional] 
**url** | **str** | The URL of this resource. | [optional] 
**links** | **object** | Nested resource URLs. | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_service_entity import VerifyV2ServiceEntity

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2ServiceEntity from a JSON string
verify_v2_service_entity_instance = VerifyV2ServiceEntity.from_json(json)
# print the JSON string representation of the object
print VerifyV2ServiceEntity.to_json()

# convert the object into a dict
verify_v2_service_entity_dict = verify_v2_service_entity_instance.to_dict()
# create an instance of VerifyV2ServiceEntity from a dict
verify_v2_service_entity_form_dict = verify_v2_service_entity.from_dict(verify_v2_service_entity_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


