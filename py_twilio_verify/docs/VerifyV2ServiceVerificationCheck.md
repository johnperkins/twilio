# VerifyV2ServiceVerificationCheck


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sid** | **str** | The unique string that identifies the resource | [optional] 
**service_sid** | **str** | The SID of the Service that the resource is associated with | [optional] 
**account_sid** | **str** | The SID of the Account that created the resource | [optional] 
**to** | **str** | The phone number or email being verified | [optional] 
**channel** | [**VerificationCheckEnumChannel**](VerificationCheckEnumChannel.md) |  | [optional] 
**status** | **str** | The status of the verification resource | [optional] 
**valid** | **bool** | Whether the verification was successful | [optional] 
**amount** | **str** | The amount of the associated PSD2 compliant transaction. | [optional] 
**payee** | **str** | The payee of the associated PSD2 compliant transaction | [optional] 
**date_created** | **datetime** | The ISO 8601 date and time in GMT when the Verification Check resource was created | [optional] 
**date_updated** | **datetime** | The ISO 8601 date and time in GMT when the Verification Check resource was last updated | [optional] 
**sna_attempts_error_codes** | **List[object]** | List of error codes as a result of attempting a verification using the &#x60;sna&#x60; channel. | [optional] 

## Example

```python
from py_twilio_verify.models.verify_v2_service_verification_check import VerifyV2ServiceVerificationCheck

# TODO update the JSON string below
json = "{}"
# create an instance of VerifyV2ServiceVerificationCheck from a JSON string
verify_v2_service_verification_check_instance = VerifyV2ServiceVerificationCheck.from_json(json)
# print the JSON string representation of the object
print VerifyV2ServiceVerificationCheck.to_json()

# convert the object into a dict
verify_v2_service_verification_check_dict = verify_v2_service_verification_check_instance.to_dict()
# create an instance of VerifyV2ServiceVerificationCheck from a dict
verify_v2_service_verification_check_form_dict = verify_v2_service_verification_check.from_dict(verify_v2_service_verification_check_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


