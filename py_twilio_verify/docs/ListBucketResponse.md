# ListBucketResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**buckets** | [**List[VerifyV2ServiceRateLimitBucket]**](VerifyV2ServiceRateLimitBucket.md) |  | [optional] 
**meta** | [**ListBucketResponseMeta**](ListBucketResponseMeta.md) |  | [optional] 

## Example

```python
from py_twilio_verify.models.list_bucket_response import ListBucketResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ListBucketResponse from a JSON string
list_bucket_response_instance = ListBucketResponse.from_json(json)
# print the JSON string representation of the object
print ListBucketResponse.to_json()

# convert the object into a dict
list_bucket_response_dict = list_bucket_response_instance.to_dict()
# create an instance of ListBucketResponse from a dict
list_bucket_response_form_dict = list_bucket_response.from_dict(list_bucket_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


