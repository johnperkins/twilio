# ListWebhookResponse


## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**webhooks** | [**List[VerifyV2ServiceWebhook]**](VerifyV2ServiceWebhook.md) |  | [optional] 
**meta** | [**ListBucketResponseMeta**](ListBucketResponseMeta.md) |  | [optional] 

## Example

```python
from py_twilio_verify.models.list_webhook_response import ListWebhookResponse

# TODO update the JSON string below
json = "{}"
# create an instance of ListWebhookResponse from a JSON string
list_webhook_response_instance = ListWebhookResponse.from_json(json)
# print the JSON string representation of the object
print ListWebhookResponse.to_json()

# convert the object into a dict
list_webhook_response_dict = list_webhook_response_instance.to_dict()
# create an instance of ListWebhookResponse from a dict
list_webhook_response_form_dict = list_webhook_response.from_dict(list_webhook_response_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


