# coding: utf-8

"""
    Twilio - Verify

    This is the public Twilio REST API.  # noqa: E501

    The version of the OpenAPI document: 1.37.3
    Contact: support@twilio.com
    Generated by: https://openapi-generator.tech
"""


from __future__ import annotations
from inspect import getfullargspec
import pprint
import re  # noqa: F401
import json

from datetime import datetime
from typing import Optional
from pydantic import BaseModel, Field, StrictStr, constr, validator

class VerifyV2ServiceMessagingConfiguration(BaseModel):
    """NOTE: This class is auto generated by OpenAPI Generator.
    Ref: https://openapi-generator.tech

    Do not edit the class manually.
    """
    account_sid: Optional[constr(strict=True, max_length=34, min_length=34)] = Field(None, description="The SID of the Account that created the resource")
    service_sid: Optional[constr(strict=True, max_length=34, min_length=34)] = Field(None, description="The SID of the Service that the resource is associated with")
    country: Optional[StrictStr] = Field(None, description="The ISO-3166-1 country code of the country or `all`.")
    messaging_service_sid: Optional[constr(strict=True, max_length=34, min_length=34)] = Field(None, description="The SID of the Messaging Service used for this configuration.")
    date_created: Optional[datetime] = Field(None, description="The RFC 2822 date and time in GMT when the resource was created")
    date_updated: Optional[datetime] = Field(None, description="The RFC 2822 date and time in GMT when the resource was last updated")
    url: Optional[StrictStr] = Field(None, description="The URL of this resource.")
    __properties = ["account_sid", "service_sid", "country", "messaging_service_sid", "date_created", "date_updated", "url"]

    @validator('account_sid')
    def account_sid_validate_regular_expression(cls, v):
        if not re.match(r"^AC[0-9a-fA-F]{32}$", v):
            raise ValueError(r"must validate the regular expression /^AC[0-9a-fA-F]{32}$/")
        return v

    @validator('service_sid')
    def service_sid_validate_regular_expression(cls, v):
        if not re.match(r"^VA[0-9a-fA-F]{32}$", v):
            raise ValueError(r"must validate the regular expression /^VA[0-9a-fA-F]{32}$/")
        return v

    @validator('messaging_service_sid')
    def messaging_service_sid_validate_regular_expression(cls, v):
        if not re.match(r"^MG[0-9a-fA-F]{32}$", v):
            raise ValueError(r"must validate the regular expression /^MG[0-9a-fA-F]{32}$/")
        return v

    class Config:
        allow_population_by_field_name = True
        validate_assignment = True

    def to_str(self) -> str:
        """Returns the string representation of the model using alias"""
        return pprint.pformat(self.to_dict())

    def to_json(self) -> str:
        """Returns the JSON representation of the model using alias"""
        return json.dumps(self.to_dict())

    @classmethod
    def from_json(cls, json_str: str) -> VerifyV2ServiceMessagingConfiguration:
        """Create an instance of VerifyV2ServiceMessagingConfiguration from a JSON string"""
        return cls.from_dict(json.loads(json_str))

    def to_dict(self):
        """Returns the dictionary representation of the model using alias"""
        _dict = self.dict(by_alias=True,
                          exclude={
                          },
                          exclude_none=True)
        # set to None if account_sid (nullable) is None
        if self.account_sid is None:
            _dict['account_sid'] = None

        # set to None if service_sid (nullable) is None
        if self.service_sid is None:
            _dict['service_sid'] = None

        # set to None if country (nullable) is None
        if self.country is None:
            _dict['country'] = None

        # set to None if messaging_service_sid (nullable) is None
        if self.messaging_service_sid is None:
            _dict['messaging_service_sid'] = None

        # set to None if date_created (nullable) is None
        if self.date_created is None:
            _dict['date_created'] = None

        # set to None if date_updated (nullable) is None
        if self.date_updated is None:
            _dict['date_updated'] = None

        # set to None if url (nullable) is None
        if self.url is None:
            _dict['url'] = None

        return _dict

    @classmethod
    def from_dict(cls, obj: dict) -> VerifyV2ServiceMessagingConfiguration:
        """Create an instance of VerifyV2ServiceMessagingConfiguration from a dict"""
        if obj is None:
            return None

        if type(obj) is not dict:
            return VerifyV2ServiceMessagingConfiguration.parse_obj(obj)

        _obj = VerifyV2ServiceMessagingConfiguration.parse_obj({
            "account_sid": obj.get("account_sid"),
            "service_sid": obj.get("service_sid"),
            "country": obj.get("country"),
            "messaging_service_sid": obj.get("messaging_service_sid"),
            "date_created": obj.get("date_created"),
            "date_updated": obj.get("date_updated"),
            "url": obj.get("url")
        })
        return _obj

